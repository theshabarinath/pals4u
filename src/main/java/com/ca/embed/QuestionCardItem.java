package com.ca.embed;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.ca.constants.Difficulty;
import com.ca.constants.QuestionType;


@Getter @Setter
public class QuestionCardItem extends CardItem{
	private String content;
	private String imageUrl;
	private Difficulty difficulty;
	private QuestionType type;
	private List<Option> options;
	private QSolution solution;

}
