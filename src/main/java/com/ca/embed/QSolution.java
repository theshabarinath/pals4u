package com.ca.embed;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class QSolution implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String answer;
	private String solution;
	private String imageUrl;

}
