package com.ca.embed;

import lombok.Getter;
import lombok.Setter;

import com.ca.constants.SubjectItemType;

@Getter @Setter
public class SubjectItem {
	
	private String id;
	private String title;
	private String logoUrl;
	private String qtnsCount;
	private int seqId;
	private String description;
	private long duration; //milli sec
	private SubjectItemType itemType;
	private int totolMarks;

}
