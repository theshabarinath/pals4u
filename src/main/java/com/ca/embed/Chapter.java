package com.ca.embed;

import lombok.Getter;
import lombok.Setter;

import org.mongodb.morphia.annotations.Embedded;

import com.ca.constants.Group;

@Embedded
@Getter @Setter
public class Chapter {
	private String title;
	private String logoUrl;
	private String seqId;
	private String id;
	private Group group;

}
