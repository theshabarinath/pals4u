package com.ca.embed;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TheoryCardItem extends CardItem {
	
	private String content;
	private String title;
}
