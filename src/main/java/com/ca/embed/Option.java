package com.ca.embed;

import lombok.Getter;
import lombok.Setter;


@Getter @Setter
public class Option {

	private String id;
	private String content;
	private String imageUrl;
	private boolean ans;
	
}
