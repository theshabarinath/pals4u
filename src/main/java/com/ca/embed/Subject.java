package com.ca.embed;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;


@Getter @Setter
public class Subject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String title;
	private int seqId;
	private String logoUrl;
	private List<Chapter> chaptersList;
}
