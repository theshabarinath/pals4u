package com.ca.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import com.ca.model.User;
import com.ca.service.UserService;

public class SecurityInspector {
	private static String AUTH_SESSION_USER = "user";
	
	@Autowired
	private UserService userService;
	
	
	public static User getSessionUser(HttpServletRequest request){
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute(AUTH_SESSION_USER);
		//User user = (User)request.getAttribute(AUTH_SESSION_USER);
		return user;
	}
	
	public static User getSessionUser(HttpServletRequest request,UserService userSrvc){
		HttpSession session = request.getSession();
		if(request.getHeader("APICLIENT")!=null && request.getHeader("APICLIENT").equals("ANDROID")){
			String userId = request.getHeader("userId");
			String accessToken = request.getHeader("accessToken");
			return userSrvc.getUserByAccessToken(userId,accessToken);
		}
		else{
			User user = (User)session.getAttribute(AUTH_SESSION_USER);
		//User user = (User)request.getAttribute(AUTH_SESSION_USER);
			return user;
		}
	}
	
	public static void signInUser(HttpServletRequest request,User user){
		HttpSession session = request.getSession();
		session.putValue(AUTH_SESSION_USER, user);
	}
}
