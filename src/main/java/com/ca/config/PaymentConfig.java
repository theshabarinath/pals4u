package com.ca.config;

import lombok.Getter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class PaymentConfig {


	@Value("${paytm.merchant.id}")
	private String paytmMerchantId;
	
	@Value("${paytm.merchant.key}")
	private String paytmMerchantKey;
	
	@Value("${paytm.callbackuri}")
	private String paytmCallBackUrl;
	
	
	
	@Value("${paytm.industryid}")
	private String paytmIndustryId;
	
	@Value("${paytm.mcallbackuri}")
	private String paytmMCallbackUri;
	
	@Value("${paytm.webchannelid}")
	private String paytmWebChannelId;
	
	@Value("${paytm.appchannelid}")
	private String paytmAppChannelId;
	
	@Value("${paytm.web.website}")
	private String paytmWebWebsite;
	
	@Value("${paytm.app.website}")
	private String paytmAppWebsite;
	
	@Value("${paytm.transactionUrl}")
	private String paytmTransactionUrl;

	
	
	

	
	
}
