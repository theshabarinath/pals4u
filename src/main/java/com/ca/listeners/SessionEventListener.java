package com.ca.listeners;

import java.util.UUID;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionEventListener implements HttpSessionListener{
	
	public static final String CSRF_TOKEN = "csrf_token";

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		// TODO Auto-generated method stub
		HttpSession session = se.getSession();
		session.putValue(CSRF_TOKEN, UUID.randomUUID().toString());
		session.setMaxInactiveInterval(24*60*60);
		
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		// TODO Auto-generated method stub
		HttpSession s = se.getSession();
		//s.removeAttribute(CSRF_TOKEN);
		
	}

}
