package com.ca.utils;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ServiceException extends Exception{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String errorMessage;
	private int errorCode;

	public ServiceException(String error){
		
		this.errorMessage = error;
	}
}
