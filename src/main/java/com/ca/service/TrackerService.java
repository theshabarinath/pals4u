package com.ca.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.ca.dao.ChapterTrackerDao;
import com.ca.dao.CourseTrackingDao;
import com.ca.dao.PatronChapterDao;
import com.ca.dao.SubjectDao;
import com.ca.embed.Chapter;
import com.ca.model.CardSetTracking;
import com.ca.model.ChapterTracking;
import com.ca.model.CourseTracking;
import com.ca.model.PatronChapter;
import com.ca.model.PerformanceSummary;
import com.ca.model.Subject;
import com.ca.model.User;
import com.google.gson.Gson;

@Service
public class TrackerService extends GenericService{
	
	
	@Autowired
	private ChapterTrackerDao dTracker;
	
	@Autowired
	private PatronChapterDao dPatronChapter;
	
	@Autowired
	private SubjectDao dSubject;
	
	
	
	
	@Autowired
	private CourseTrackingDao dCourseTracking;
	
	public void updateChapterTracker(CardSetTracking cardsetTrack,String patronChapterId,String cardsetId,User user){
		ChapterTracking tracker = dTracker.getChapterTrackerOfUser(user.getId(), patronChapterId);
		PatronChapter patronChapter = dPatronChapter.findById(patronChapterId);
		
		System.out.println("called tow");
		if(tracker==null){
			
			tracker = new ChapterTracking();
			tracker.setId(ObjectId.get().toString());
			tracker.setCreatedOn(new Date());
			tracker.setChapterId(patronChapter.getChapterId());
			tracker.setPatronChapterId(patronChapterId);
			tracker.setSubjectId(patronChapter.getSubjectId());
			tracker.setCourseId(patronChapter.getCourseId());
			tracker.setUserId(user.getId());
			
			cardsetTrack.setLastVisitedOn(new Date().getTime());
		
			Map<String,CardSetTracking> map = new HashMap();
			map.put(cardsetId, cardsetTrack);
			
			tracker.setCardsetTrackingList(map);
			
		}
		else{
			
			Map<String,CardSetTracking> map = tracker.getCardsetTrackingList();
			if(map.containsKey(cardsetId)){
				CardSetTracking oldTracking = map.get(cardsetId);
				oldTracking.setAccuracy(cardsetTrack.getAccuracy());
				oldTracking.setProgress(cardsetTrack.getProgress());
				oldTracking.setRightAttempts(cardsetTrack.getRightAttempts());
				oldTracking.setWrongAttempts(cardsetTrack.getWrongAttempts());
				if(oldTracking.getReadCardIds()!=null && cardsetTrack.getReadCardIds()!=null)
					oldTracking.getReadCardIds().addAll(cardsetTrack.getReadCardIds());
				
				oldTracking.setLastVisitedOn(new Date().getTime());
				for(String key : cardsetTrack.getCardTracking().keySet()){
					boolean found = false;
					for(String oldKey : oldTracking.getCardTracking().keySet()){
						if(oldKey.equals(key)){
							found = true;
							oldTracking.getCardTracking().put(oldKey, cardsetTrack.getCardTracking().get(key));
						}
							
					}
					if(!found){
						oldTracking.getCardTracking().put(key, cardsetTrack.getCardTracking().get(key));
					}
				}
				
				tracker.getCardsetTrackingList().put(cardsetId, oldTracking);
				dTracker.save(tracker);
			}
			else{
				cardsetTrack.setLastVisitedOn(new Date().getTime());
				tracker.getCardsetTrackingList().put(cardsetId, cardsetTrack);
			}
		}
		
		float progress= 0;
		float accuracy = 0;
		for(String key : tracker.getCardsetTrackingList().keySet()){
			progress = progress + tracker.getCardsetTrackingList().get(key).getProgress();
			accuracy = accuracy + tracker.getCardsetTrackingList().get(key).getAccuracy();
		}
		progress = progress/patronChapter.getCardSetsList().size();
		accuracy  = accuracy / tracker.getCardsetTrackingList().size();
		
		tracker.setProgress(progress);
		tracker.setAccuracy(accuracy);
		
		System.out.println(new Gson().toJson(tracker));
		dTracker.save(tracker);
		updateCourseTracker(user,tracker);
	}
	
	@Async
	private void updateCourseTracker(User user,ChapterTracking tracker){
		
		System.out.println(new Gson().toJson(tracker));
		CourseTracking cTracking = dCourseTracking.findById(tracker.getCourseId(), user.getId());
		List<Subject> subjectsList = dSubject.getSubjectsByCourse(tracker.getCourseId());
		System.out.println(new Gson().toJson(subjectsList));
		if(cTracking==null){
			cTracking = new CourseTracking();
			cTracking.setId(ObjectId.get().toString());
			cTracking.setCourseId(tracker.getCourseId());
			cTracking.setUserId(user.getId());
			cTracking.setCreatedOn(new Date());
			
		}
		
		if(cTracking.getSubSummary()==null){
			Map<String,PerformanceSummary> map = new HashMap();
			cTracking.setSubSummary(map);
			
		}
		if(cTracking.getChapSummary()==null){
			Map<String,PerformanceSummary> map = new HashMap();
			cTracking.setChapSummary(map);
			
		}
		
		if(!cTracking.getChapSummary().containsKey(tracker.getChapterId())){
			PerformanceSummary summary = new PerformanceSummary();
			cTracking.getChapSummary().put(tracker.getChapterId(), summary);
			
			//PerformanceSummary summary = cTracking.getChapSummary().get(tracker.getChapterId());
			
		}
			
			PerformanceSummary chapSummary = cTracking.getChapSummary().get(tracker.getChapterId());
			chapSummary.setAccuracy(tracker.getAccuracy());
			chapSummary.setProgress(tracker.getProgress());
			
			int count = 0;
			for(String key : tracker.getCardsetTrackingList().keySet()){
				
				count = count + (tracker.getCardsetTrackingList().get(key).getReadCardIds()==null ?0:tracker.getCardsetTrackingList().get(key).getReadCardIds().size());
			
			}
			chapSummary.setCardsRead(count);
			cTracking.getChapSummary().put(tracker.getChapterId(), chapSummary);
	
		
		if(!cTracking.getSubSummary().containsKey(tracker.getSubjectId())){
			PerformanceSummary s = new PerformanceSummary();
			cTracking.getSubSummary().put(tracker.getSubjectId(), s);
		}
		
		PerformanceSummary sum = cTracking.getSubSummary().get(tracker.getSubjectId());
		
		int chaptersLength=0;
		float accuracy = 0;
		int chaptersExists= 0;
		int cardsRead = 0;
		for(Subject sub: subjectsList){
			if(sub.getId().equals(tracker.getSubjectId())){
				chaptersLength = sub.getChapters().size();
				
				for(Chapter chap: sub.getChapters()){
					if(cTracking.getChapSummary().containsKey(chap.getId())){
						accuracy = accuracy + cTracking.getChapSummary().get(chap.getId()).getAccuracy();
						chaptersExists = chaptersExists +1;
						cardsRead  = cardsRead + cTracking.getChapSummary().get(chap.getId()).getCardsRead();
					}
				}
			}
		}
		accuracy  = tracker.getAccuracy()/chaptersExists;
		
		sum.setProgress(sum.getProgress()+(tracker.getProgress()/chaptersLength));
		sum.setAccuracy(sum.getAccuracy()+accuracy);
		
		sum.setCardsRead(cardsRead);
		
		cTracking.getSubSummary().put(tracker.getSubjectId(), sum);
		System.out.println("ourse tracking");
		System.out.println(new Gson().toJson(cTracking));
		dCourseTracking.saveCourse(cTracking);
	}
	
	public CourseTracking getCourseTracking(String courseId,String userId){
		return dCourseTracking.findById(courseId, userId);
	}
	
	public ChapterTracking getChapterTracking(String patronChapterId,String userId){
		return dTracker.getChapterTrackerOfUser(userId, patronChapterId);
	}
}
