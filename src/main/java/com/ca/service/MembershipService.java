package com.ca.service;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ca.constants.TxnStatus;
import com.ca.dao.CouponDao;
import com.ca.dao.StudentPurchaseDao;
import com.ca.dao.UserDao;
import com.ca.model.Coupon;
import com.ca.model.Coupon.CouponType;
import com.ca.model.CoursePackage;
import com.ca.model.PackagePlan;
import com.ca.model.StudentPurchase;
import com.ca.model.User;
import com.ca.utils.ServiceException;

@Service
public class MembershipService extends GenericService{

	@Autowired
	private StudentPurchaseDao dPurchase;
	
	@Autowired
	private CouponDao dCoupon;
	
	@Autowired
	private UserDao dUser;
	
	
	
	public float applyCoupon(String couponCode,String purchaseId) throws ServiceException{
		Coupon coupon = dCoupon.getCouponByCode(couponCode);
		
		if(coupon==null)
			throw new ServiceException("coupon dosnot exists");
		StudentPurchase purchase = dPurchase.findById(purchaseId);
		float amount = purchase.getNetAmount();
		float discountAmount = 0;
		if(coupon.getCouponType().toString().equals(CouponType.AMOUNT.toString())){
			discountAmount = coupon.getDiscount();
		}
		
		if(coupon.getCouponType().toString().equals(CouponType.PERCENT.toString())){
			discountAmount = (amount *coupon.getDiscount())/100;
		}
		
		return discountAmount;
	
	}
	
	public StudentPurchase createStudentPurchase(CoursePackage pkg,User user,float amount){
		
		StudentPurchase purchase = new StudentPurchase();
		purchase.setId(ObjectId.get().toString());
		purchase.setCreatedOn(new Date());
		purchase.setPkg(pkg);
		purchase.setNetAmount(amount);
		purchase.setUserId(user.getId());
		dPurchase.save(purchase);
		return purchase;
	
	}
	
	public StudentPurchase updatePurchase(String purchaseId,float netAmount){
			StudentPurchase purchase = dPurchase.findById(purchaseId);
			purchase.setNetAmount(netAmount);
			dPurchase.save(purchase);
			return purchase;
	}
	
	public User handelPaytmForMobiles(User user,String txId,String purchaseId,TxnStatus status){
		StudentPurchase sp = dPurchase.findById(purchaseId);
		sp.setTransactionId(txId);
		sp.setTxnStatus(status);
	
		if(sp.getTxnStatus().equals(TxnStatus.COMPLETED)){
			
			long expireTime = new Date().getTime()+(365*24*60*60*1000);
			
			
			PackagePlan plan = new PackagePlan();
			plan.setPkgType(sp.getPkg().getPkgType());
			plan.setPkgTitle(sp.getPkg().getTitle());
			plan.setExpiresOn(new Date(expireTime));   
			
			sp.setPkgPlan(plan);
			user.setPkg(sp.getPkgPlan());
			dUser.saveUser(user);
		
		}

		dPurchase.save(sp);
		return user;
		
	}
	
}
