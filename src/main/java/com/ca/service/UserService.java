package com.ca.service;

import java.util.Date;
import java.util.UUID;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ca.dao.UserAuthDao;
import com.ca.dao.UserDao;
import com.ca.model.User;
import com.ca.model.UserAuth;
import com.ca.model.UserAuth.AuthInterface;
import com.ca.util.StringUtil;
import com.ca.utils.ServiceException;
import com.google.gson.Gson;

@Service
public class UserService extends GenericService{
	
	
	@Autowired
	private UserAuthDao dUserAuth;
	
	@Autowired
	private UserDao dUser;
	
	public User getUserByAccessToken(String userId,String accessToken){
		UserAuth auth = dUserAuth.getByAccessToken(userId, accessToken);
		if(auth==null)
			return null;
		else{
			return dUser.findById(userId);
		}
			
					
	}
	
	public String createUserAuth(String userId,AuthInterface authInterface){
		UserAuth auth = new UserAuth();
		auth.setId(ObjectId.get().toString());
		auth.setUserId(userId);
		auth.setCreatedOn(new Date());
		auth.setAccessToken(UUID.randomUUID().toString());
		auth.setAuthInterface(authInterface);
		dUserAuth.saveUserAuth(auth);
		return auth.getAccessToken();
	}
	
	public User saveUser(String userId,String email,String phone,String name) throws ServiceException{
		
		if(!StringUtil.isEmailValid(email))
			throw new ServiceException("Invalid Email");
		
		if(!StringUtil.isPhoneValid(phone))
			throw new ServiceException("Invalid Phone");
		
		if(StringUtil.empty(name))
			throw new ServiceException("Name cannot be empty");
		
		User exUser = dUser.getStudentUserByEmail(email);
		if(exUser !=null && !exUser.getId().equals(userId))
			throw new ServiceException("Email already exists");
		
		exUser = dUser.getStudentUserByPhone(phone);
		if(exUser!=null && !exUser.getId().equals(userId))
			throw new ServiceException("Phone already exists");
		
		User user = dUser.findById(userId);
		user.setFirstName(name);
		user.setEmail(email);
		user.setPhone(phone);
		
		dUser.saveUser(user);
		return user;
	}
	
	public void UpdatePswd(User user,String npswd,String rnpswd) throws ServiceException{
		if(StringUtil.empty(rnpswd) || StringUtil.empty(npswd))
			throw new ServiceException("Cannot be empty");
		
		if(!npswd.equals(rnpswd))
			throw new ServiceException("passwords are not matching");
		
		user.setPassword(new StandardPasswordEncoder().encode(npswd));
		dUser.saveUser(user);
	}
	
	public void forgotPassword(String email) throws ServiceException{
		User user = dUser.getStudentUserByEmail(email);
		if(user==null)
			throw new ServiceException("email does not exists");
		
		String str = StringUtil.getRandomPswd(user.getEmail());
		System.out.println(str);
		user.setPassword(new StandardPasswordEncoder().encode(str));
		dUser.saveUser(user);
	}

}
