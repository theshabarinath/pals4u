package com.ca.service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ca.constants.Role;
import com.ca.constants.SignupSource;
import com.ca.dao.UserDao;
import com.ca.model.User;
import com.ca.util.StringUtil;
import com.ca.utils.ServiceException;
import com.google.api.client.auth.openidconnect.IdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.gson.Gson;

@Service
public class AuthService extends GenericService{
	
	@Autowired
	private UserDao dUser;
	
	private final static String CLIENT_ID= "591517660815-eeoum0ci7e1558s6692sorqa7jkmaejh.apps.googleusercontent.com";
	
	
	public User createAccount(String email,String password,String name,SignupSource source) throws ServiceException{
		
		if(!StringUtil.isEmailValid(email))
			throw new ServiceException("enter valid email");
		
		if(password==null || password.length()<6)
			throw new ServiceException("password should be of minimum 6 letters");
		User user = new User();
		user.setId(ObjectId.get().toString());
		user.setSource(source);
		
		user.setPassword(new StandardPasswordEncoder().encode(password));
		
		user.setEmail(email);
		user.setFirstName(name);
		user.setCreatedOn(new Date());
		List<Role> list = new ArrayList();
		list.add(Role.STUDENT);
		user.setRoles(list);
		dUser.saveUser(user);
		return user;
	}
	
	public User verifyLogin(String username,String password) throws ServiceException{
		if(username==null || username.isEmpty())
			throw new ServiceException("empty string bro");
		
		if(password==null || password.isEmpty())
			throw new ServiceException("password should be minimum 5 letters");
		
		User user = dUser.getBackendUserByEmail(username);
		//System.out.println()
		System.out.println(new Gson().toJson(user));
		StandardPasswordEncoder encoder = new StandardPasswordEncoder();
		//if(encoder.matches(password, user.getPassword())){
			return user;
	//	}
	//	else
		//	throw new ServiceException("password is wrong");
		
//		User user = new User();
//		user.setId(ObjectId.get().toString());
//		user.setFirstName("Shabari Nath");
//		user.setCreatedOn(new Date());
//		user.setEmail(username);
//		StandardPasswordEncoder encoder = new StandardPasswordEncoder();
//		user.setPassword(encoder.encode(password));
//		List<Role> list = new ArrayList();
//		list.add(Role.ADMIN);
//		dUser.saveUser(user);
//		return user;
	
	}
	
	
	public User verifyStudentLogin(String username,String password) throws ServiceException{
		if(username==null || username.isEmpty())
			throw new ServiceException("empty string bro");
		
		if(password==null || password.isEmpty())
			throw new ServiceException("password should be minimum 5 letters");
		
		User user = dUser.getStudentUserByEmail(username);
	
		return user;
	
	}
	
	public User verifyGoogleToken(String token) throws ServiceException{
		GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), JacksonFactory.getDefaultInstance())
	    // Specify the CLIENT_ID of the app that accesses the backend:
	    .setAudience(Collections.singletonList(CLIENT_ID))
	    // Or, if multiple clients access the backend:
	    //.setAudience(Arrays.asList(CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3))
	    .build();

	// (Receive idTokenString by HTTPS POST)

	GoogleIdToken idToken;
	try {
		idToken = verifier.verify(token);
		if (idToken != null) {
			  Payload payload = idToken.getPayload();

			  // Print user identifier
			  String userId = payload.getSubject();
			  System.out.println("User ID: " + userId);

			  // Get profile information from payload
			 // String email = payload.getEmail();
			  //boolean emailVerified = Boolean.valueOf(payload.getEmailVerified());
			 
			  String email = (String) payload.get("email");
			  String name = (String) payload.get("name");
			  String pictureUrl = (String) payload.get("picture");
			  String locale = (String) payload.get("locale");
			  String familyName = (String) payload.get("family_name");
			  String givenName = (String) payload.get("given_name");
			  User user = dUser.getStudentUserByEmail(email);
			  
			  if(user!=null){
				  return user;
			  }
			  else{
				  return createAccount(email,"12345678",name,SignupSource.GOOGLE);
			  }

			  // Use or store profile information
			  // ...

			}
		else {
			  System.out.println("Invalid ID token.");
			  throw new ServiceException("invalid token");
			}
	} catch (GeneralSecurityException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		throw new ServiceException("invalid token");
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		throw new ServiceException("invalid token");
	}
	 

	}
	
	
	
	
	
}
