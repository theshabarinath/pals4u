package com.ca.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ca.constants.CardType;
import com.ca.dao.CardDao;
import com.ca.dao.CourseDao;
import com.ca.dao.PatronChapterDao;
import com.ca.dao.SubjectDao;
import com.ca.dao.TestseriesDao;
import com.ca.dao.TestseriesQuestionDao;
import com.ca.dao.UserDao;
import com.ca.embed.QuestionCardItem;
import com.ca.embed.TheoryCardItem;
import com.ca.model.Card;
import com.ca.model.CardSet;
import com.ca.model.Course;
import com.ca.model.PatronChapter;
import com.ca.model.Subject;
import com.ca.model.Testseries;
import com.ca.model.TestseriesQuestion;
import com.ca.model.User;
import com.ca.utils.ServiceException;
import com.google.gson.Gson;

@Service
public class CourseService {
	
	@Autowired
	private CourseDao dCourse;
	
	@Autowired
	private SubjectDao dSubject;
	
	@Autowired
	private TestseriesDao dTs;
	
	@Autowired
	private CardDao dCard;
	
	@Autowired
	private PatronChapterDao dChapter;
	
	@Autowired
	private TestseriesQuestionDao dTsQtn;
	
	@Autowired
	private UserDao dUser;
	
	public List<Subject> getSubjectsByCourseId(String courseId){
		return dSubject.getSubjectsByCourse(courseId);
	}

	public Course getCourse(String courseId){
		System.out.println("getting course"+courseId);
		System.out.println(dCourse.findById(courseId));
		return dCourse.findById(courseId);
	}
	
	public List<Course> getCoursesOfUser(String userId){
		return dCourse.getCoursesOfUser(userId);
	}
	
	public List<PatronChapter> getPatronChapters(User user){
		return dChapter.getPatronChapters(user.getId());
	}
	
	public List<Card> getCardsOfCardset(String cardsetId){
		return dCard.getCardsList(cardsetId);
	}
	
	public List<Card> getQtnCardsList(String parentId){
		return dCard.getQtnCardsList(parentId);
	}
	
	public List<Course> getCourses(){
		List<Course> list = dCourse.getCoursesList();
//		for(Course c: list){
//			c.setSubjects(getSubjectsByCourseId(c.getId()));
//		}
		System.out.println(new Gson().toJson(list));
		return list;
	}
	
	public PatronChapter saveChapter(User user ,PatronChapter chapter){
		chapter.setId(ObjectId.get().toString());
		chapter.setPatronId(user.getId());
		chapter.setPatronName(user.getFirstName());
		System.out.println(new Gson().toJson(chapter));
		dChapter.saveChapter(chapter);
		return chapter;
	}
	
	
	
	public CardSet updateCardsSets(CardSet cardset,String patronChapId,User user){
		System.out.println("print");
		System.out.println(patronChapId);
		PatronChapter chap = dChapter.findById(patronChapId);
		System.out.println(chap.getTitle());
		if(chap.getCardSetsList()!=null){
			if(cardset.getId()!=null){
				for(CardSet set: chap.getCardSetsList()){
					if(set.getId().equals(cardset.getId())){
						set.setTitle(cardset.getTitle());
						set.setSeqId(cardset.getSeqId());
						set.setCardIds(cardset.getCardIds());
					}
				}
				
			}
			else{
				cardset.setId(ObjectId.get().toString());
				chap.getCardSetsList().add(cardset);
			}
		}
		else{
			cardset.setId(ObjectId.get().toString());
			chap.setCardSetsList(new ArrayList());
			chap.getCardSetsList().add(cardset);
		}
		
		dChapter.saveChapter(chap);
		return cardset;
	}
	
	public Card addTheoryCard(User user,TheoryCardItem item,String chapterId,String cardsetId,String cardId,String subjectId,String patronChapterId){
		if(cardId!=null){
			Card oCard = dCard.findById(cardId);
			oCard.setChapterId(chapterId);
			oCard.setSubjectId(subjectId);
			oCard.setCardItem(item);
			dCard.save(oCard);
			return oCard;
			
		}
		else{
			Card card = new Card();
			card.setId(ObjectId.get().toString());
			card.setChapterId(chapterId);
			card.setSubjectId(subjectId);
			card.setCardSetId(cardsetId);
			card.setPatronChapterId(patronChapterId);
			card.setPatronId(user.getId());
			card.setCardItem(item);
			card.setCardType(CardType.THEORY);
			dCard.save(card);
			updateCardsets(patronChapterId,cardsetId,card.getId());
			return card;
			
		}
		
		
	}
	
	public void deleteCard(User user,String cardId,String patronChapterId){
		dCard.delete(cardId);
		PatronChapter chapter = dChapter.findById(patronChapterId);
		for(CardSet cardSet: chapter.getCardSetsList()){
			cardSet.getCardIds().remove(cardId);
//			for(String id: cardSet.getCardIds()){
//				if(id.equals(cardId)){
//					cardSet.getCardIds().remove(cardId);
//					break;
//				}
//			}
		}
		dChapter.saveChapter(chapter);
	}
	
	
	public void deleteCardset(User user ,String cardsetId,String patronChapterId) throws ServiceException{
		PatronChapter chapter = dChapter.findById(patronChapterId);
		for(CardSet cardSet: chapter.getCardSetsList()){
			if(cardSet.getId().equals(cardsetId)){
				if(cardSet.getCardIds()!=null){
					System.out.println(new Gson().toJson(cardSet));
					throw new ServiceException("cards exists in this cardsets. delete them");
				}
				
				chapter.getCardSetsList().remove(cardSet);
				break;
			}
		}
		dChapter.saveChapter(chapter);
		
	}
	
	public void deleteChapter(User user,String patronChapterId) throws ServiceException{
		PatronChapter chapter = dChapter.findById(patronChapterId);
		System.out.println(new Gson().toJson(chapter.getCardSetsList()));
		if(chapter.getCardSetsList()!=null || (chapter.getCardSetsList()!=null && chapter.getCardSetsList().size()!=0))
			throw new ServiceException("cardsets exists");
		dChapter.delete(patronChapterId);
		
	}
	
	public PatronChapter getRandomPatronChapter(String chapterId,User user){
		PatronChapter chap = dChapter.getRandonPatron(chapterId);
		if(user.getSubscribedPatrons()==null){
			user.setSubscribedPatrons(new HashMap());
		}
		user.getSubscribedPatrons().put(chapterId,chap.getId() );
		dUser.saveUser(user);
		return chap;
		
	}
	
	public User selectCourse(User user,String title){
		Course course = dCourse.findByTitle(title);
		user.setCourseId(course.getId());
		dUser.saveUser(user);
		return user;
		
	}
	
	public PatronChapter getPatronChapterById(String patronChapterId){
		return dChapter.findById(patronChapterId);
	}
	
	public Card addQtnCard(User user,QuestionCardItem item,String chapterId,String cardsetId,String cardId,String subjectId,String patronChapterId,String parentId){
		if(cardId!=null){
			Card oCard = dCard.findById(cardId);
			oCard.setChapterId(chapterId);
			oCard.setSubjectId(subjectId);
			oCard.setCardItem(item);
			
			dCard.save(oCard);
			return oCard;
			
		}
		else{
			Card card = new Card();
			card.setId(ObjectId.get().toString());
			card.setChapterId(chapterId);
			card.setSubjectId(subjectId);
			card.setCardSetId(cardsetId);
			card.setParentId(parentId);
			card.setPatronChapterId(patronChapterId);
			card.setPatronId(user.getId());
			card.setCardItem(item);
			card.setCardType(CardType.QUESTION);
			dCard.save(card);
			updateCardsets(patronChapterId,cardsetId,card.getId());
			return card;
			
		}
		
		
	}
	
	
	public void deleteQtnCard(String cardId){
		dCard.deleteCardById(cardId);
	}
	
	private void updateCardsets(String patronChapterId,String cardsetId,String cardId){
		PatronChapter chap = dChapter.findById(patronChapterId);
		if(chap.getCardSetsList()!=null){
			for(CardSet set: chap.getCardSetsList()){
				if(set.getId().equals(cardsetId)){
					if(set.getCardIds()==null)
						set.setCardIds(new ArrayList());
					set.getCardIds().add(cardId);
				}
			}
		}
		dChapter.saveChapter(chap);
	}
	
//	public Course saveCourse(Course course,User user){
//		if(course.getId()==null){
//			course.setId(ObjectId.get().toString());
//			course.setByPatronId(user.getId());
//			course.setByPatronImage(user.getProfileIcon());
//			course.setByPatronName(user.getFirstName());
//			course.setCreatedOn(new Date());
//			dCourse.saveCourse(course);
//		}
//		else{
//			Course nCourse = dCourse.findById(course.getId());
//			nCourse.setCost(course.getCost());
//			nCourse.setTitle(course.getTitle());
//			dCourse.saveCourse(nCourse);
//		}
//		return course;
//	}
//	
//	public Subject saveSubject(Subject subject,User user,String courseId){
//		if(subject.getId()==null){
//			subject.setId(ObjectId.get().toString());
//			subject.setCreatedOn(new Date());
//			subject.setCourseId(courseId);
//			dSubject.saveSubject(subject);
//		
//		}
//		else{
//			Subject nSub = dSubject.findById(subject.getId());
//			nSub.setTitle(subject.getTitle());
//			nSub.setLogoUrl(subject.getLogoUrl());
//			nSub.setSeqId(subject.getSeqId());
//			dSubject.saveSubject(nSub);
//		}
//		return subject;
//	}
//	
//	public SubjectItem updateChapter(String subjectId,SubjectItem chap){
//		if(chap.getId()==null){
//			chap.setId(ObjectId.get().toString());
//			dSubject.updateItemsList(chap, subjectId);
//			createTestseries(chap,User user);
//		}
//		else{
//			Subject sub = dSubject.findById(subjectId);	
//			if(sub.getItemsList()!=null){
//				for(SubjectItem ch: sub.getItemsList()){
//					if(ch.getId().equals(chap.getId())){
//						ch.setLogoUrl(chap.getLogoUrl());
//						ch.setSeqId(chap.getSeqId());
//						ch.setDescription(chap.getDescription());
//						ch.setTitle(chap.getTitle());
//					}
//				}
//			}
//			dSubject.saveSubject(sub);
//		}
//		return chap;
//	}
	
	public Testseries createTestseries(Testseries ts,User user){
		if(ts.getId()==null){
		
			ts.setId(ObjectId.get().toString());
			ts.setCreatedOn(new Date());
			ts.setCreatedBy(user.getEmail());
			dTs.save(ts);
			return ts;
		}
		else{
			Testseries t = dTs.findById(ts.getId());
			t.setChapters(ts.getChapters());
			t.setCourseId(ts.getCourseId());
			t.setDuration(ts.getDuration());
			t.setQuestionIds(ts.getQuestionIds());
			t.setSubjects(ts.getSubjects());
			t.setTestType(ts.getTestType());
			t.setTotalMarks(ts.getTotalMarks());
			dTs.save(t);
			return t;
		}
		
		
	}
	
	public TestseriesQuestion createQuestion(TestseriesQuestion tsq,User user,String tsid){
		if(tsq.getId()!=null){
			TestseriesQuestion q = dTsQtn.findById(tsq.getId());
			tsq.setId(q.getId());
			dTsQtn.save(tsq);
			return tsq;
		}
		else{
			tsq.setId(ObjectId.get().toString());
			tsq.setCreatedOn(new Date());
			tsq.setCreatedBy(user.getEmail());
		
			dTsQtn.save(tsq);
//			Testseries ts = dTs.findById(tsid);
//			if(ts.getQuestionIds()==null)
//				ts.setQuestionIds(new ArrayList());
//			ts.getQuestionIds().add(tsid);
//			dTs.save(ts);
			dTs.updateQtns(tsid, tsq.getId());
			return tsq;
			
		}
	}
	
	public List<TestseriesQuestion> getTestQuestions(String tsid){
		Testseries ts = dTs.findById(tsid);
		return dTsQtn.getQtns(ts.getQuestionIds());
	}
	
	public List<Testseries> getTests(){
		return dTs.getAllTests();
	}
	
	public Testseries findById(String id){
		return dTs.findById(id);
	}
	
	
//	public void trackCard(String patronChapterId,String cardsetId,String cardId,){
//		
//	}
	
}
