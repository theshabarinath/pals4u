package com.ca.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

import org.mongodb.morphia.annotations.Entity;

import com.ca.constants.Difficulty;
import com.ca.constants.QuestionType;
import com.ca.embed.Option;
import com.ca.embed.QSolution;

@Getter @Setter

@Entity("testseries_question")
public class TestseriesQuestion extends BaseEntity{

	private String content;
	private String imageUrl;
	private Difficulty difficulty;
	private QuestionType type;
	private List<Option> options;
	private QSolution solution;
}
