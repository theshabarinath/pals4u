package com.ca.model;

import lombok.Getter;
import lombok.Setter;

import com.ca.constants.PackageType;

@Getter @Setter
public class CoursePackage {
	
	
	private String title;
	private String description;
	private float price;
	private PackageType pkgType;
	
	
}
