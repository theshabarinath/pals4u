package com.ca.model;

import lombok.Getter;
import lombok.Setter;

import org.mongodb.morphia.annotations.Entity;


@Entity("userAuth")

@Getter @Setter
public class UserAuth extends BaseEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public enum AuthInterface{
		ANDROID,IOS,WEB
	}
	private String userId;
	private String accessToken;
	private AuthInterface authInterface;

}
