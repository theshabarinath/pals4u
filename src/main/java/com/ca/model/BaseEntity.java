package com.ca.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.PrePersist;

@Getter @Setter
public class BaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Indexed
	protected String id;
	protected String createdBy;
	protected Date createdOn;
	protected Date lastUpdated;
	
	@PrePersist
	protected void update(){
		lastUpdated = new Date();
	}
	
}
