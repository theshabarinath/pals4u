package com.ca.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

import org.mongodb.morphia.annotations.Entity;

import com.ca.constants.CardType;
import com.ca.embed.CardItem;

@Entity("individual_cards")
@Getter @Setter
public class Card extends BaseEntity{

	private String cardSetId;
	private String chapterId;
	private String patronChapterId;
	private String patronId;
	private String parentId;
	private String subjectId;
	private CardItem cardItem;
	private CardType cardType;
	private List<String> bookmarksList;
}
