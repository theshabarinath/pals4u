package com.ca.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Indexed;

import com.ca.constants.Group;
import com.ca.embed.Chapter;


@Getter @Setter

@Entity("subjects")
public class Subject extends BaseEntity {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String title;
	private String logoUrl;
	private int seqId;

	private Group group;
	private String courseId;
	private List<Chapter> chapters;
	

}
