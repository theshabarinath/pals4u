package com.ca.model;

import lombok.Getter;
import lombok.Setter;


@Getter @Setter
public class PerformanceSummary {

	private float accuracy;
	private float progress;
	private int cardsRead;
	
}
