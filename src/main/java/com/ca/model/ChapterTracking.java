package com.ca.model;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;

import org.mongodb.morphia.annotations.Entity;

@Getter @Setter
@Entity("chapter_tracking")
public class ChapterTracking extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String chapterId;
	private String patronChapterId;
	private String patronId;
	private String subjectId;
	private String courseId;
	private String userId;
	private float progress;
	private float accuracy;
	private Map<String,CardSetTracking> cardsetTrackingList;
	
	
}
