package com.ca.model;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import org.mongodb.morphia.annotations.Entity;


@Entity("coupons")
@Getter @Setter
public class Coupon extends BaseEntity{

	public enum CouponType{
		PERCENT,AMOUNT
	}
	private String coupon;
	
	private CouponType couponType;
	private float discount;
	private Date validUpto;
	
	
}
