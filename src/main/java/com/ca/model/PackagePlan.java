package com.ca.model;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import com.ca.constants.PackageType;

@Getter @Setter
public class PackagePlan{
	
	private PackageType pkgType;
	private Date boughtOn;
	private Date expiresOn;
	private String pkgTitle;

}
