package com.ca.model;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;

import org.mongodb.morphia.annotations.Entity;


@Entity("course_tracking")
@Getter @Setter
public class CourseTracking extends BaseEntity {

	
	private String userId;
	private String courseId;
	private float accuracy;
	private float progress;
	private Map<String,PerformanceSummary> subSummary;
	private Map<String,PerformanceSummary> chapSummary;
}
