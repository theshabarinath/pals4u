package com.ca.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;

import com.ca.constants.CourseType;

@Getter @Setter
@Entity("course")
public class Course extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String byPatronId;
	private String byPatronName;
	private String byPatronImage;
	private String title;
	private CourseType courseType;
	private float cost;
	private List<CoursePackage> pkgsList;
	
	@Embedded
	private List<Subject> subjects;


	
}
