package com.ca.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CardTracking {

	private String cardId;
	private int visitCount;
	private long timeSpent;
	
	//for qtn cards
	private int numOfAttempts;
	
	
}
