package com.ca.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

import org.mongodb.morphia.annotations.Entity;

import com.ca.constants.TestSeriesType;
import com.ca.embed.Chapter;

@Getter @Setter
@Entity("testseries")
public class Testseries extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int totalMarks;
	private String title;
	private List<Chapter> chapters; 
	private List<Subject> subjects;
	//in milli secs
	private long duration;
	private boolean free;
	private List<String> questionIds;
	private TestSeriesType testType;
	private String courseId;
	private List<String> attemptedStudents;
}
