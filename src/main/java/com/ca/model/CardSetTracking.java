package com.ca.model;

import java.util.Map;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CardSetTracking {

	private Set<String> readCardIds;
	private float accuracy;
	private float progress;
	private long lastVisitedOn;
	private int wrongAttempts;
	private int rightAttempts;
	private Map<String,CardTracking> cardTracking;
	
}
