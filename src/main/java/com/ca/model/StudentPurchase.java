package com.ca.model;

import java.util.TreeMap;

import lombok.Getter;
import lombok.Setter;

import org.mongodb.morphia.annotations.Entity;

import com.ca.constants.TxnStatus;

@Entity("student_purchase")
@Getter @Setter
public class StudentPurchase extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String userId;
	private String phone;
	private String email;
	private String transactionId;
	private CoursePackage pkg;
	private TxnStatus txnStatus;
	private float netAmount;
	private String couponCode;
	private PackagePlan pkgPlan;
	
	
	public static String generateCheckSumForPaytm(TreeMap<String, String> params,String merchantKey){
		CheckSumServiceHelper checksumHelper = CheckSumServiceHelper.getCheckSumServiceHelper();
		// key : Merchant Key; paramMap : TreeMap of request parameters
		String checksum="";
		try {
			checksum = checksumHelper.genrateCheckSum(merchantKey, params);
		
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		return checksum;
	}
	
	public static boolean verifyCheckSumForPaytm(TreeMap<String, String> params,String hashsum,String merchantKey){
		CheckSumServiceHelper checksumHelper = CheckSumServiceHelper.getCheckSumServiceHelper();
		// key : Merchant Key; paramMap : TreeMap of request parameters
		String checksum="";
		try {
			//checksum = checksumHelper.genrateCheckSum("V9p7G4lEdaGZyI@S", params);
			return checksumHelper.verifycheckSum(merchantKey,params, hashsum);
	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		return false;	
		}
	}

	
	
}
