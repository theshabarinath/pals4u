package com.ca.model;

import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

import org.mongodb.morphia.annotations.Entity;

import com.ca.constants.Role;
import com.ca.constants.SignupSource;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity("user")
@JsonIgnoreProperties({"password"})
@Getter @Setter
public class User extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String password;
	private SignupSource source;
	private List<Role> roles;
	private String profileIcon= "https://selfstudyadmin.s3.amazonaws.com/images/content/7da49053-013b-497f-9031-ac2fd99f472c-Profile_Pic_03.png";
	private String courseId;
	private PackagePlan pkg;
	
	private Map<String,String> subscribedPatrons;
}
