package com.ca.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

import org.mongodb.morphia.annotations.Entity;

@Getter @Setter
@Entity("patron_chapters")
public class PatronChapter extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String chapterId;
	private String subjectId;
	private String courseId;
	private String patronId;
	private String title;
	private String patronName;
	private String patronImage;
	private List<String> subscriptions;
	private List<CardSet> cardSetsList;
}
