package com.ca.ctrl;

import java.util.Collections;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

public class BaseCtrl {
	
	protected final static String DATA_KEY = "data";
	protected final static String ERROR = "error";
	
	public Map<String, ? extends Object> handleErrorWithMsg(HttpServletResponse response, String errormsg) {
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		return Collections.singletonMap(ERROR, errormsg);
	}

}
