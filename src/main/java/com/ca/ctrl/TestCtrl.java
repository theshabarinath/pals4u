package com.ca.ctrl;

import java.util.Collections;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ca.model.Testseries;
import com.ca.model.TestseriesQuestion;
import com.ca.model.User;
import com.ca.security.SecurityInspector;
import com.ca.service.CourseService;
import com.google.gson.Gson;


@Controller
public class TestCtrl extends BaseCtrl{

	@Autowired
	private CourseService sCourse;
	@RequestMapping(value="/",method=RequestMethod.GET)
	public String returnJsp(){
		return "/sample";
	}
	
	@RequestMapping(value="/privacypolicy",method=RequestMethod.GET)
	public String returnPrivacy(){
		return "/auth/privacy";
	}
	
	@RequestMapping(value="/tests",method=RequestMethod.GET)
	public String getTests(Model model,HttpServletRequest request,HttpSession session ,HttpServletResponse response){
		model.addAttribute("courses", new Gson().toJson(sCourse.getCourses()));
		return "/auth/testlist";
	}
	
	@RequestMapping(value="/tests/get/{tsid}",method=RequestMethod.GET)
	public String getTestById(@PathVariable String tsid,Model model,HttpServletRequest request,HttpSession session ,HttpServletResponse response){
		model.addAttribute("courses", new Gson().toJson(sCourse.getCourses()));
		model.addAttribute("test", new Gson().toJson(sCourse.findById(tsid)));
		return "/auth/testqtns";
	}
	
	@RequestMapping(value="/gettests",method=RequestMethod.GET)
	public @ResponseBody Map<String,?extends Object> getTestList(HttpServletRequest request,HttpSession session ,HttpServletResponse response){
		return Collections.singletonMap(DATA_KEY, sCourse.getTests());
	}
	
	@RequestMapping(value="/ts/create",method=RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> saveTest(HttpServletRequest request,HttpSession session,@RequestBody Testseries ts){
		User user = SecurityInspector.getSessionUser(request);
		return Collections.singletonMap(DATA_KEY, sCourse.createTestseries(ts, user));
	}
	
	@RequestMapping(value="/ts/saveqtn/{tsid}",method=RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> saveQuestion(@PathVariable String tsid,@RequestBody TestseriesQuestion tsq,HttpServletRequest request,HttpSession session,HttpServletResponse response){
		User user = SecurityInspector.getSessionUser(request);
		return Collections.singletonMap(DATA_KEY,sCourse.createQuestion(tsq,user,tsid));
	}
	
	@RequestMapping(value="/ts/getqtns/{tsid}",method=RequestMethod.GET)
	public @ResponseBody Map<String,?extends Object> getQuestions(@PathVariable String tsid,HttpServletRequest request,HttpSession session,HttpServletResponse response){
		User user = SecurityInspector.getSessionUser(request);
		return Collections.singletonMap(DATA_KEY,sCourse.getTestQuestions(tsid));
	}
	
	
	
	
}
