package com.ca.ctrl;

import java.util.Collections;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ca.embed.QuestionCardItem;
import com.ca.embed.TheoryCardItem;
import com.ca.model.CardSet;
import com.ca.model.CardSetTracking;
import com.ca.model.PatronChapter;
import com.ca.model.User;
import com.ca.security.SecurityInspector;
import com.ca.service.CourseService;
import com.ca.service.TrackerService;
import com.ca.service.UserService;
import com.ca.utils.ServiceException;
import com.google.gson.Gson;

@Controller
public class CourseCtrl extends BaseCtrl{
	
	@Autowired
	private UserService sUser;
	
	@Autowired
	private CourseService sCourse;
	
	@Autowired
	private TrackerService sTracker;
	
	
//	@RequestMapping(value="/m/selcourse",method=RequestMethod.POST)
//	public @ResponseBody Map<String,?extends Object> selCourse(@RequestParam String title,HttpServletRequest request,HttpServletResponse response){
//		
//	}
	@RequestMapping(value="/dashboard",method=RequestMethod.GET)
	public String getDashboard(HttpServletRequest request,HttpSession session,HttpServletResponse response){
		User user = SecurityInspector.getSessionUser(request);
		if(user==null)
			return "/auth/signin";
		else
			return "/auth/dashboard";
	}
	
	@RequestMapping(value="/createchapter",method=RequestMethod.GET)
	public String createChapter(Model model,HttpServletRequest request,HttpSession session,HttpServletResponse response){
		User user = SecurityInspector.getSessionUser(request);
		if(user==null)
			return "/auth/signin";
		else{
			System.out.println("tess");
			model.addAttribute("courseList", new Gson().toJson(sCourse.getCourses()));
			return "/auth/cardset";
		}
	}
	
	@RequestMapping(value="/addcardqtns",method=RequestMethod.GET)
	public String addCardQtns(Model model,HttpServletRequest request,HttpSession session,HttpServletResponse response){
		User user = SecurityInspector.getSessionUser(request);
		if(user==null)
			return "/auth/signin";
		else{
			System.out.println("tess");
			return "/auth/addqtn";
		}
	}
	
	
	@RequestMapping(value="/sp/getsubjects/{courseId}",method=RequestMethod.GET)
	public @ResponseBody Map<String,?extends Object> getSubjects(@PathVariable String courseId,HttpServletRequest request){
		User user = SecurityInspector.getSessionUser(request,sUser);
		if(user==null)
			return null;
		else{
			return Collections.singletonMap(DATA_KEY, sCourse.getCourse(courseId));
			
		}
	}
	
	@RequestMapping(value="/sp/getcourses",method=RequestMethod.GET)
	public @ResponseBody Map<String,?extends Object> getCourses(HttpServletRequest request){
		User user = SecurityInspector.getSessionUser(request);
		if(user==null)
			return null;
		else
		{
			return Collections.singletonMap(DATA_KEY, sCourse.getCoursesOfUser(user.getId()));
		}
	}
	
	@RequestMapping(value="/sp/getchapters",method=RequestMethod.GET)
	public @ResponseBody Map<String,?extends Object> getPatronChapters(HttpServletRequest request){
		User user = SecurityInspector.getSessionUser(request);
		if(user==null)
			return null;
		else
		{
			return Collections.singletonMap(DATA_KEY, sCourse.getPatronChapters(user));
		}
	}
	
	@RequestMapping(value="/sp/getcards/{cardsetId}",method=RequestMethod.GET)
	public @ResponseBody Map<String,?extends Object> getCardsOfCardset(@PathVariable String cardsetId,HttpServletRequest request){
		User user = SecurityInspector.getSessionUser(request,sUser);
		if(user==null)
			return null;
		else
		{
			return Collections.singletonMap(DATA_KEY, sCourse.getCardsOfCardset(cardsetId));
		}
	}
	
	@RequestMapping(value="/sp/getqtncards/{parentId}",method=RequestMethod.GET)
	public @ResponseBody Map<String,?extends Object> getQtnsCardsOfCardset(@PathVariable String parentId,HttpServletRequest request){
		User user = SecurityInspector.getSessionUser(request);
		if(user==null)
			return null;
		else
		{
			return Collections.singletonMap(DATA_KEY, sCourse.getQtnCardsList(parentId));
		}
	}
	
	@RequestMapping(value="/sp/savepatronchapter",method=RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> saveChapterByPatron(@RequestBody PatronChapter chapter,HttpServletRequest request,HttpServletResponse response,HttpSession session){
		User user = SecurityInspector.getSessionUser(request);
		if(user==null)
			return null;
		else
		{
			System.out.println("chapter saving");
			return Collections.singletonMap(DATA_KEY, sCourse.saveChapter(user, chapter));
		}
	}
	
	
	
	@RequestMapping(value="/sp/savecardset/{patronChapterId}",method=RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> saveCardSet(@PathVariable String patronChapterId,@RequestBody CardSet cardSet,HttpServletRequest request,HttpServletResponse response,HttpSession session){
		User user = SecurityInspector.getSessionUser(request);
		if(user==null)
			return null;
		else
		{
			return Collections.singletonMap(DATA_KEY, sCourse.updateCardsSets(cardSet, patronChapterId, user));
		}
	}
	
	@RequestMapping(value="/sp/savetheorycard/{subjectId}/{chapterId}/{patronChapterId}/{cardsetId}",method=RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> saveCard(@PathVariable String patronChapterId,@RequestParam(required=false) String cardId,@PathVariable String cardsetId,@PathVariable String subjectId,@PathVariable String chapterId,@RequestBody TheoryCardItem cardItem,HttpServletRequest request,HttpServletResponse response,HttpSession session){
		User user = SecurityInspector.getSessionUser(request);
		if(user==null)
			return null;
		else
		{
			return Collections.singletonMap(DATA_KEY, sCourse.addTheoryCard(user, cardItem, chapterId, cardsetId, cardId, subjectId,patronChapterId));
		}
	}
	
	@RequestMapping(value="/sp/deletecard/{cardId}/{patronChapterId}",method=RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> deleteCard(@PathVariable String patronChapterId,@PathVariable String cardId,HttpServletRequest request,HttpServletResponse response,HttpSession session){
		User user = SecurityInspector.getSessionUser(request);
		if(user==null)
			return null;
		else
		{
			sCourse.deleteCard(user, cardId, patronChapterId);
			return Collections.singletonMap(DATA_KEY, "ss");
		}
	}
	
	
	@RequestMapping(value="/sp/deletepatronchap/{chapId}",method=RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> deletePatronChap(@PathVariable String chapId,HttpServletRequest request,HttpServletResponse response,HttpSession session){
		User user = SecurityInspector.getSessionUser(request);
		if(user==null)
			return null;
		else
		{
			try {
				sCourse.deleteChapter(user, chapId);
				return Collections.singletonMap(DATA_KEY, "success");
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage()+"error");
				return handleErrorWithMsg(response,e.getMessage());
			}
			
		}
	}
	
	@RequestMapping(value="/sp/deletecardset/{cardsetId}/{chapId}",method=RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> deleteCardset(@PathVariable String cardsetId,@PathVariable String chapId,HttpServletRequest request,HttpServletResponse response,HttpSession session){
		User user = SecurityInspector.getSessionUser(request);
		if(user==null)
			return null;
		else
		{
			try {
				sCourse.deleteCardset(user, cardsetId, chapId);
				return Collections.singletonMap(DATA_KEY, "success");
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				return handleErrorWithMsg(response,e.getMessage());
			}
			
		}
	}
	
	
	
	@RequestMapping(value="/sp/saveqtncard/{subjectId}/{chapterId}/{patronChapterId}/{cardsetId}/{parentId}",method=RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> saveCard(@PathVariable String parentId,@PathVariable String patronChapterId,@RequestParam(required=false) String cardId,@PathVariable String cardsetId,@PathVariable String subjectId,@PathVariable String chapterId,@RequestBody QuestionCardItem cardItem,HttpServletRequest request,HttpServletResponse response,HttpSession session){
		User user = SecurityInspector.getSessionUser(request);
		if(user==null)
			return null;
		else
		{
			return Collections.singletonMap(DATA_KEY, sCourse.addQtnCard(user, cardItem, chapterId, cardsetId, cardId, subjectId,patronChapterId,parentId));
		}
	}
	
	@RequestMapping(value="/sp/deleteqtncard/{cardId}",method=RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> deleteCard(@PathVariable String cardId,HttpServletRequest request,HttpServletResponse response,HttpSession session){
		User user = SecurityInspector.getSessionUser(request);
		if(user==null)
			return null;
		else
		{
			sCourse.deleteQtnCard(cardId);
			return Collections.singletonMap(DATA_KEY, "SUCCESS");
		}
	}
	
	@RequestMapping(value="/m/sp/getrandom/{chapterId}",method=RequestMethod.GET)
	public @ResponseBody Map<String,?extends Object> getChapter(@PathVariable String chapterId,HttpServletRequest request,HttpSession session,HttpServletResponse response){
		User user = SecurityInspector.getSessionUser(request,sUser);
		if(user==null)
			return null;
		else
		{
			return Collections.singletonMap(DATA_KEY, sCourse.getRandomPatronChapter(chapterId, user));
		}
	}
	
	@RequestMapping(value="/m/sp/selectcourse",method=RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> selectCourse(@RequestParam String title,HttpServletRequest request,HttpSession session,HttpServletResponse response){
		User user = SecurityInspector.getSessionUser(request,sUser);
		if(user==null)
			return null;
		else
		{
			return Collections.singletonMap(DATA_KEY, sCourse.selectCourse(user, title));
		}
	}
	
	@RequestMapping(value="/m/sp/getpatronchapter/{patronChapterId}",method=RequestMethod.GET)
	public @ResponseBody Map<String,?extends Object> getPatronChapter(@PathVariable String patronChapterId,HttpServletRequest request,HttpSession session,HttpServletResponse response){
		User user = SecurityInspector.getSessionUser(request,sUser);
		if(user==null)
			return null;
		else
		{
			return Collections.singletonMap(DATA_KEY, sCourse.getPatronChapterById(patronChapterId));
		}
	}
	
	
	@RequestMapping(value="/m/sp/tracking/{chapterId}/{cardsetId}",method=RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> trackCard(@RequestBody CardSetTracking cardsetTrack,@RequestParam String patronChapterId,@PathVariable String chapterId,@PathVariable String cardsetId,HttpServletRequest request,HttpSession session,HttpServletResponse response){
		User user = SecurityInspector.getSessionUser(request,sUser);
		if(user==null)
			return null;
		else
		{
			sTracker.updateChapterTracker(cardsetTrack, patronChapterId, cardsetId, user);
			return Collections.singletonMap(DATA_KEY, "SUCCESS");
		}
	}
	
	@RequestMapping(value="/m/sp/getcoursetracking",method=RequestMethod.GET)
	public @ResponseBody Map<String,?extends Object> getCourseTracking(HttpServletRequest request,HttpSession session,HttpServletResponse response){
		User user = SecurityInspector.getSessionUser(request,sUser);
		if(user==null)
			return null;
		else
		{
			return Collections.singletonMap(DATA_KEY, sTracker.getCourseTracking(user.getCourseId(), user.getId()));
		}
	}
	
	@RequestMapping(value="/m/sp/getchaptertracking/{patronChapterId}",method=RequestMethod.GET)
	public @ResponseBody Map<String,?extends Object> getChapterTracking(@PathVariable String patronChapterId,HttpServletRequest request,HttpSession session,HttpServletResponse response){
		User user = SecurityInspector.getSessionUser(request,sUser);
		if(user==null)
			return null;
		else
		{
			return Collections.singletonMap(DATA_KEY, sTracker.getChapterTracking(patronChapterId, user.getId()));
		}
	}
	

	
	
	
	
	
//	@RequestMapping(value="/sp/savecourse",method=RequestMethod.POST)
//	public @ResponseBody Map<String,?extends Object> saveCourse(@RequestBody Course course,HttpServletRequest request,HttpSession session,HttpServletResponse response){
//		User user = SecurityInspector.getSessionUser(request);
//		if(user==null)
//			return null;
//		else{
//			return Collections.singletonMap(DATA_KEY, sCourse.saveCourse(course, user));
//			
//		}
//				
//	}
//	
//	@RequestMapping(value="/sp/savesubject/{courseId}",method=RequestMethod.POST)
//	public @ResponseBody Map<String,?extends Object> saveCourse(@PathVariable String courseId,@RequestBody Subject subject,HttpServletRequest request,HttpSession session,HttpServletResponse response){
//		User user = SecurityInspector.getSessionUser(request);
//		if(user==null)
//			return null;
//		else{
//			return Collections.singletonMap(DATA_KEY, sCourse.saveSubject(subject, user,courseId));
//			
//		}
//				
//	}
//	
//	@RequestMapping(value="/sp/addItem/{subjectId}",method=RequestMethod.POST)
//	public @ResponseBody Map<String,?extends Object> saveCourse(@PathVariable String subjectId,@RequestBody SubjectItem item,HttpServletRequest request,HttpSession session,HttpServletResponse response){
//		User user = SecurityInspector.getSessionUser(request);
//		if(user==null)
//			return null;
//		else{
//			return Collections.singletonMap(DATA_KEY, sCourse.updateChapter(subjectId, item));
//			
//		}
//				
//	}
}
