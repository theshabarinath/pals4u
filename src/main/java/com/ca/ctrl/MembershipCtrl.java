package com.ca.ctrl;

import java.util.Collections;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ca.config.PaymentConfig;
import com.ca.constants.TxnStatus;
import com.ca.model.CoursePackage;
import com.ca.model.StudentPurchase;
import com.ca.model.User;
import com.ca.security.SecurityInspector;
import com.ca.service.MembershipService;
import com.ca.service.UserService;
import com.ca.util.StringUtil;
import com.ca.utils.ServiceException;
import com.google.gson.Gson;

@Controller
public class MembershipCtrl extends BaseCtrl{
	
	@Autowired
	private UserService sUser;
	
	@Autowired
	private MembershipService sMember;
	
	@Autowired
	private PaymentConfig paymentConfig;
	
	@RequestMapping(value="/m/applycoupon",method=RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> applyCoupon(@RequestParam String couponCode,@RequestParam String purchaseId,HttpServletRequest request,HttpSession session,HttpServletResponse response){
		
		try {
			User user = SecurityInspector.getSessionUser(request,sUser);
			
			return Collections.singletonMap(DATA_KEY,sMember.applyCoupon(couponCode, purchaseId) );
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return handleErrorWithMsg(response,e.getMessage());
		}
		
	}
	
	@RequestMapping(value="/m/createstudentpurchase",method=RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> createPurchase(@RequestParam float amount,@RequestBody CoursePackage pkg,HttpServletRequest request,HttpSession session,HttpServletResponse response){
		
		
			User user = SecurityInspector.getSessionUser(request,sUser);	
			return Collections.singletonMap(DATA_KEY,sMember.createStudentPurchase(pkg, user, amount));
		
		
	}
	
	
	@RequestMapping(value = "/m/paytmmoneyreq", method = RequestMethod.GET)
	public @ResponseBody Map<String,?extends Object> getPaytmFormForMobile(@RequestParam String amount,@RequestParam String purchaseId,Model model, HttpServletResponse response,
			HttpServletRequest request, HttpSession session) {
		Random rand = new Random();
		String rndm = Integer.toString(rand.nextInt())+(System.currentTimeMillis() / 1000L);
		String txnid=StringUtil.hashCal("SHA-256",rndm).substring(0,20);
		StudentPurchase purchase = sMember.updatePurchase(purchaseId, Float.parseFloat(amount));
		TreeMap<String, String> params = new TreeMap<String, String>();
		params.put("REQUEST_TYPE", "DEFAULT");
		params.put("MID", paymentConfig.getPaytmMerchantId());
		params.put("ORDER_ID",ObjectId.get().toString());
		//params.put("CUST_ID",request.getHeader("userId"));
		params.put("CUST_ID",ObjectId.get().toString());
		//params.put("TXN_AMOUNT", String.valueOf(pp.getNetAmount()));
		params.put("TXN_AMOUNT",amount);
		params.put("CHANNEL_ID", paymentConfig.getPaytmAppChannelId());
		params.put("INDUSTRY_TYPE_ID",paymentConfig.getPaytmIndustryId());
		params.put("THEME","merchant");
		params.put("WEBSITE",paymentConfig.getPaytmAppWebsite());
		
		params.put("MOBILE_NO",purchase.getPhone());
		params.put("EMAIL", purchase.getEmail());
		
	
		// testing params.put("CALLBACK_URL", "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp");
		 params.put("CALLBACK_URL", "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="+params.get("ORDER_ID"));
			
		String hash = StudentPurchase.generateCheckSumForPaytm(params,paymentConfig.getPaytmMerchantKey());
		params.put("CHECKSUMHASH", hash);
		System.out.println(new Gson().toJson(params));
		return params;
	}
	
	@RequestMapping(value = "/m/paytmcallback", method = RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> getPaytmCallbackForMobile(@RequestParam TxnStatus status,@RequestParam String TXNID,@RequestParam String purchaseId,Model model, HttpServletResponse response,
			HttpServletRequest request, HttpSession session) {
		
		User user = SecurityInspector.getSessionUser(request,sUser);
		sMember.handelPaytmForMobiles(user, TXNID, purchaseId, status);
		return Collections.singletonMap(DATA_KEY,"SUCCESS");
		
	}

}
