package com.ca.ctrl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ca.constants.SignupSource;
import com.ca.model.User;
import com.ca.model.UserAuth.AuthInterface;
import com.ca.security.SecurityInspector;
import com.ca.service.AuthService;
import com.ca.service.UserService;
import com.ca.utils.ServiceException;

@Controller
public class AuthCtrl extends BaseCtrl{
	
	@Autowired
	private AuthService sAuth;
	
	@Autowired
	private UserService sUser;
	
	@RequestMapping(value="/signin",method=RequestMethod.GET)
	public String getSignin(HttpServletRequest request,HttpSession session,HttpServletResponse response){
		return "/auth/signin";
	}
	
	@RequestMapping(value="/verifysignin",method=RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> verifySignin(@RequestParam String username,@RequestParam String password,HttpServletRequest request,HttpSession session,HttpServletResponse response){
		try {
			User user = sAuth.verifyLogin(username, password);
			SecurityInspector.signInUser(request, user);
			return Collections.singletonMap(DATA_KEY, user);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Collections.singletonMap(DATA_KEY, "success");
		}
	}
	
	
	
	
	@RequestMapping(value="/m/verifygoogletoken",method=RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> verifyGoogleToken(@RequestParam String token,HttpServletRequest request,HttpSession session,HttpServletResponse response){
		try {
			User user = sAuth.verifyGoogleToken(token);
			String accessToken = sUser.createUserAuth(user.getId(), AuthInterface.ANDROID);
			Map<String,Object> map = new HashMap();
			map.put("accessToken", accessToken);
			map.put("user",user);
			return map;
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Collections.singletonMap(DATA_KEY, "success");
		}
	}
	
	@RequestMapping(value="/m/verifystudentsignin",method=RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> verifyStudentSignin(@RequestParam String username,@RequestParam String password,HttpServletRequest request,HttpSession session,HttpServletResponse response){
		try {
			System.out.println("ssss");
			User user = sAuth.verifyStudentLogin(username, password);
			String accessToken = sUser.createUserAuth(user.getId(), AuthInterface.ANDROID);
			Map<String,Object> map = new HashMap();
			map.put("accessToken", accessToken);
			map.put("user",user);
			return map;
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Collections.singletonMap(DATA_KEY, "success");
		}
	}
	
	
	@RequestMapping(value="/m/createaccount",method=RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> createAccount(@RequestParam SignupSource source,@RequestParam String email,@RequestParam String password,@RequestParam String name,HttpServletRequest request,HttpSession session,HttpServletResponse response){
		//User user = SecurityInspector.getSessionUser(request,sUser);
		try {
			System.out.println("syvvfuiylf");
			User user = sAuth.createAccount(email, password, name,source);
			String accessToken = sUser.createUserAuth(user.getId(), AuthInterface.ANDROID);
			Map<String,Object> map = new HashMap();
			map.put("accessToken", accessToken);
			map.put("user",user);
			return map;
			//return Collections.singletonMap(DATA_KEY, user);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
			return Collections.singletonMap(ERROR, e.getMessage());
		}
		
	}
}
