package com.ca.ctrl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ca.constants.AppConstants;
import com.ca.model.User;
import com.ca.security.SecurityInspector;
import com.ca.service.UserService;
import com.ca.utils.ServiceException;

@Controller
public class UserCtrl extends BaseCtrl{
	
	@Autowired
	private UserService sUser;
	
	@RequestMapping(value="/m/profileversion",method=RequestMethod.GET)
	public @ResponseBody Map<String,?extends Object> checkVersion(HttpServletRequest request,HttpSession session,HttpServletResponse response){
		User user = SecurityInspector.getSessionUser(request,sUser);
		Map<String,Object> map = new HashMap();
		map.put("user",user);
		map.put("version", AppConstants.ANDROID_APP_VERSION);
		return map;
	}
	
	@RequestMapping(value="/m/saveuserdetails",method=RequestMethod.POST)
	public @ResponseBody Map<String,? extends Object> saveUser(HttpServletResponse response,HttpServletRequest request,@RequestParam String name,@RequestParam String email,@RequestParam String phone){
		User user = SecurityInspector.getSessionUser(request,sUser);
		try {
			user = sUser.saveUser(user.getId(), email, phone, name);
			return Collections.singletonMap(DATA_KEY,user );
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			System.out.println("error service");
			e.printStackTrace();
			System.out.println(e.getMessage());
			return handleErrorWithMsg(response,e.getMessage());
		}
		
	}
	
	@RequestMapping(value="/m/changepswd",method=RequestMethod.POST)
	public @ResponseBody Map<String,? extends Object> changePswd(HttpServletResponse response,HttpServletRequest request,@RequestParam String npswd,@RequestParam String rnpswd){
		User user = SecurityInspector.getSessionUser(request,sUser);
		try {
			sUser.UpdatePswd(user, npswd, rnpswd);
			return Collections.singletonMap(DATA_KEY,"SUCCESS" );
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			
			return handleErrorWithMsg(response,e.getMessage());
		}
		
	}
	
	@RequestMapping(value="/m/forgotpswd/getotp",method=RequestMethod.POST)
	public @ResponseBody Map<String,?extends Object> forgetPasswordAndGetOtp(@RequestParam String email,HttpServletRequest request,HttpSession session,HttpServletResponse response){
		try {
			sUser.forgotPassword(email);
			return Collections.singletonMap(DATA_KEY, "success");
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return handleErrorWithMsg(response,e.getErrorMessage());
		}
		
	}
	

}
