package com.ca.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.regex.Pattern;

public class StringUtil {
	
	public static final String EMAIL_PATTERN = "^(.+)@(.+)$";
	public static final Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);
	
	public static final String PHONE_PATTERN = "^[6789]\\d{9}$";
	public static final Pattern phonePattern = Pattern.compile(PHONE_PATTERN);
	
	public static boolean isEmailValid(String email){
		return email!=null && emailPattern.matcher(email).matches();
	}
	
	public static String hashCal(String type,String str){
		byte[] hashseq=str.getBytes();
		StringBuffer hexString = new StringBuffer();
		try{
		MessageDigest algorithm = MessageDigest.getInstance(type);
		algorithm.reset();
		algorithm.update(hashseq);
		byte messageDigest[] = algorithm.digest();
            
		

		for (int i=0;i<messageDigest.length;i++) {
			String hex=Integer.toHexString(0xFF & messageDigest[i]);
			if(hex.length()==1) hexString.append("0");
			hexString.append(hex);
		}
			
		}catch(NoSuchAlgorithmException nsae){ }
		
		return hexString.toString();
	}
	
	public static String getRandomPswd(String username){
		String refCode;
		if(username==null || username.length()<3){
			refCode = getRandomNumber(100, 9999)+"";
		}else{
			refCode = username.substring(0, 3);
		}
		refCode=refCode+ getRandomNumber(100, 9999);
		return refCode;
	}
	
	private static int getRandomNumber(int min, int max){
		Random random = new Random();
		return random.nextInt((max - min) + 1) + min;
	}
	
	
	public static boolean empty(String s)
	{
		if(s== null || s.trim().equals(""))
			return true;
		else
			return false;
	}
	
	public static boolean isPhoneValid(String phone){
		return phone!=null && phonePattern.matcher(phone).matches();
	}

}
