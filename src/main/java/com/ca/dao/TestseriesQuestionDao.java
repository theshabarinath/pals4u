package com.ca.dao;

import java.util.List;

import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ca.model.TestseriesQuestion;

@Repository
public class TestseriesQuestionDao {
	
	@Autowired
	private MongoDbFactory mf;
	
	public void save(TestseriesQuestion qtn){
		Query<TestseriesQuestion> q= mf.getDatastore().createQuery(TestseriesQuestion.class);
		mf.getDatastore().save(qtn);
	}
	
	public TestseriesQuestion findById(String qId){
		Query<TestseriesQuestion> q= mf.getDatastore().createQuery(TestseriesQuestion.class);
		q.field("id").equal(qId);
		return q.get();
	}
	
	public List<TestseriesQuestion> getQtns(List<String> qIds){
		Query<TestseriesQuestion> q= mf.getDatastore().createQuery(TestseriesQuestion.class);
		q.field("id").in(qIds);
		return q.asList();
	}

}
