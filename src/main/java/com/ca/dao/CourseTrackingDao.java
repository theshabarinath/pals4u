package com.ca.dao;

import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ca.model.Course;
import com.ca.model.CourseTracking;

@Repository
public class CourseTrackingDao {

	
	@Autowired
	private MongoDbFactory mf;
	
	public void saveCourse(CourseTracking tracking){
		Query<CourseTracking> q = mf.getDatastore().createQuery(CourseTracking.class);
		mf.getDatastore().save(tracking);
	}
	
	public CourseTracking findById(String courseId,String userId){
		Query<CourseTracking> q = mf.getDatastore().createQuery(CourseTracking.class);
		q.field("courseId").equal(courseId);
		q.field("userId").equal(userId);
		return q.get();
	}
}
