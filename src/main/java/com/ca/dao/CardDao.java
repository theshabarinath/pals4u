package com.ca.dao;

import java.util.List;

import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ca.model.Card;
import com.ca.model.Course;


@Repository
public class CardDao {
	
	@Autowired
	private MongoDbFactory mf;
	
	public Card findById(String id){
		Query<Card> q = mf.getDatastore().createQuery(Card.class);
		q.field("id").equal(id);
		return q.get();
	}
	public void save(Card card){
		Query<Card> q = mf.getDatastore().createQuery(Card.class);
		mf.getDatastore().save(card);
	}
	
	public List<Card> getCardsList(String cardsetId){
		Query<Card> q = mf.getDatastore().createQuery(Card.class);
		q.field("cardSetId").equal(cardsetId);
		return q.asList();
		
	}
	
	public void deleteCardById(String cardId){
		Query<Card> q = mf.getDatastore().createQuery(Card.class);
		q.field("id").equal(cardId);
		mf.getDatastore().delete(q);
	}
	
	public List<Card> getQtnCardsList(String parentId){
		Query<Card> q = mf.getDatastore().createQuery(Card.class);
		q.field("cardType").equal("QUESTION");
		q.field("parentId").equal(parentId);
		return q.asList();
		
		
		
	}
	
	public void delete(String cardId){
		Query<Card> q = mf.getDatastore().createQuery(Card.class);
		q.field("id").equal(cardId);
		mf.getDatastore().delete(q);
	}

}
