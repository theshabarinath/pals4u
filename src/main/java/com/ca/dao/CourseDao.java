package com.ca.dao;

import java.util.List;

import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ca.model.Course;


@Repository
public class CourseDao {

	@Autowired
	private MongoDbFactory mf;
	
	public void saveCourse(Course course){
		Query<Course> q = mf.getDatastore().createQuery(Course.class);
		mf.getDatastore().save(course);
	}
	
	public List<Course> getCoursesList(){
		Query<Course> q = mf.getDatastore().createQuery(Course.class);
		return q.asList();
	}
	
	public Course findById(String courseId){
		Query<Course> q = mf.getDatastore().createQuery(Course.class);
		q.field("_id").equal(courseId);
		return q.get();
	}
	
	public Course findByTitle(String title){
		Query<Course> q = mf.getDatastore().createQuery(Course.class);
		q.field("title").equal(title);
		return q.get();
	}
	
	public List<Course> getCoursesOfUser(String userId){
		Query<Course> q = mf.getDatastore().createQuery(Course.class);
		q.field("byPatronId").equal(userId);
		return q.asList();
	}
}
