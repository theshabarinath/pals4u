package com.ca.dao;

import javax.annotation.PostConstruct;

import lombok.Getter;
import lombok.Setter;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.springframework.stereotype.Component;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

@Getter @Setter
@Component
public class MongoDbFactory {

	private String dbName = "pals4udb";
	private String uri = "mongodb://pals4u:pals4u@ds261253.mlab.com:61253/pals4udb";
	
	private Datastore datastore;
	private MongoClient mongoClient;
	private Morphia morphia;
	
	@PostConstruct
	public void init(){
			mongoClient = new MongoClient(new MongoClientURI(uri));
			morphia = new Morphia();
			datastore =  morphia.createDatastore(mongoClient, dbName);
			datastore.ensureIndexes();
	}
}

