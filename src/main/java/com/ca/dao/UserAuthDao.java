package com.ca.dao;

import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ca.model.Course;
import com.ca.model.UserAuth;

@Repository
public class UserAuthDao {
	@Autowired
	private MongoDbFactory mf;
	
	
	public void saveUserAuth(UserAuth userAuth){
		Query<UserAuth> q = mf.getDatastore().createQuery(UserAuth.class);
		mf.getDatastore().save(userAuth);
	}
	
	public UserAuth getByAccessToken(String userId,String accessToken){
		Query<UserAuth> q = mf.getDatastore().createQuery(UserAuth.class);
		q.field("userId").equal(userId);
		q.field("accessToken").equal(accessToken);
		return q.get();
	}

}
