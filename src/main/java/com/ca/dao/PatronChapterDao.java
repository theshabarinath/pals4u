package com.ca.dao;

import java.util.List;
import java.util.Random;

import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ca.model.PatronChapter;

@Repository
public class PatronChapterDao {
	
	@Autowired
	private MongoDbFactory mf;
	
	public void saveChapter(PatronChapter chap){
		Query<PatronChapter> q = mf.getDatastore().createQuery(PatronChapter.class);
		mf.getDatastore().save(chap);
	}
	
	public PatronChapter findById(String id){
		Query<PatronChapter> q = mf.getDatastore().createQuery(PatronChapter.class);
		q.field("id").equal(id);
		return q.get();
	}
	
	public void delete(String patronChapterId){
		Query<PatronChapter> q = mf.getDatastore().createQuery(PatronChapter.class);
		q.field("id").equal(patronChapterId);
		mf.getDatastore().delete(q);
	}
	
	public List<PatronChapter> getPatronChapters(String userId){
		Query<PatronChapter> q = mf.getDatastore().createQuery(PatronChapter.class);
		q.field("patronId").equal(userId);
		return q.asList();
	}
	
	public PatronChapter getRandonPatron(String chapterId){
		Query<PatronChapter> q = mf.getDatastore().createQuery(PatronChapter.class);
		q.field("chapterId").equal(chapterId);
		int size = (int)q.countAll();
		int random = new Random().nextInt(size);
		q.offset(random);
		q.limit(1);
		return q.get();
	}
	
	

}
