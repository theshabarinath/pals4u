package com.ca.dao;


import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ca.model.ChapterTracking;

@Repository
public class ChapterTrackerDao {

	@Autowired
	private MongoDbFactory mf;
	
	public ChapterTracking getChapterTrackerOfUser(String userId,String patronChapterId){
		Query<ChapterTracking> q = mf.getDatastore().createQuery(ChapterTracking.class);
		q.field("userId").equal(userId);
		q.field("patronChapterId").equal(patronChapterId);
		return q.get();
	}
	
	public void save(ChapterTracking tracking){
		mf.getDatastore().save(tracking);
	}
}
