package com.ca.dao;

import java.util.List;

import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ca.model.Testseries;


@Repository
public class TestseriesDao {

	@Autowired
	private MongoDbFactory mf;
	
	public void save(Testseries ts){
		Query<Testseries> q = mf.getDatastore().createQuery(Testseries.class);
		mf.getDatastore().save(ts);
	}
	
	public Testseries findById(String id){
		Query<Testseries> q = mf.getDatastore().createQuery(Testseries.class);
		q.field("id").equal(id);
		return q.get();
	}
	
	public List<Testseries> getAllTests(){
		Query<Testseries> q = mf.getDatastore().createQuery(Testseries.class);
		return q.asList();
	}
	
	public void updateQtns(String tsId,String qId){
		Query<Testseries> q = mf.getDatastore().createQuery(Testseries.class);
		q.field("id").equal(tsId);
		mf.getDatastore().update(q, mf.getDatastore().createUpdateOperations(Testseries.class).add("questionIds", qId,false));
	}
	
}
