package com.ca.dao;

import java.util.Date;

import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ca.model.Coupon;


@Repository
public class CouponDao {

	@Autowired
	private MongoDbFactory mf;
	
	public Coupon getCouponByCode(String couponCode){
		Query<Coupon> q = mf.getDatastore().createQuery(Coupon.class);
		q.field("coupon").equal(couponCode);
		//q.field("validUpto").greaterThanOrEq(new Date());
		return q.get();
	}
}
