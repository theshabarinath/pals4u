package com.ca.dao;

import java.util.List;

import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ca.embed.SubjectItem;
import com.ca.model.Subject;

@Repository
public class SubjectDao {

	@Autowired
	private MongoDbFactory mf;
	
	public void saveSubject(Subject subject){
		Query<Subject> q = mf.getDatastore().createQuery(Subject.class);
		mf.getDatastore().save(subject);
	}
	
	public Subject findById(String subjectId){
		Query<Subject> q = mf.getDatastore().createQuery(Subject.class);
		q.field("id").equal(subjectId);
		return q.get();
	}
	
	public List<Subject> getSubjectsByCourse(String courseId){
		Query<Subject> q = mf.getDatastore().createQuery(Subject.class);
		q.field("courseId").equal(courseId);
		return q.asList();
	}
	
	
	
	public void updateItemsList(SubjectItem item,String subjectId){
		Query<Subject> q = mf.getDatastore().createQuery(Subject.class);
		q.field("id").equal(subjectId);
		mf.getDatastore().update(q, mf.getDatastore().createUpdateOperations(Subject.class).add("itemsList", item, false));
	}
}
