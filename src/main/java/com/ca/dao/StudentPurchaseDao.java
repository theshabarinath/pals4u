package com.ca.dao;

import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ca.model.StudentPurchase;


@Repository
public class StudentPurchaseDao {

	@Autowired
	private MongoDbFactory mf;
	
	public void save(StudentPurchase purchase){
		mf.getDatastore().save(purchase);
	}
	
	public StudentPurchase findById(String purchaseId){
		Query<StudentPurchase> q = mf.getDatastore().createQuery(StudentPurchase.class);
		q.field("id").equal(purchaseId);
		return q.get();
	}
}
