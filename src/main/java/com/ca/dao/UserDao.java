package com.ca.dao;

import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ca.constants.Role;
import com.ca.model.User;

@Repository
public class UserDao {
	
	@Autowired
	private MongoDbFactory mf;
	
	public void saveUser(User user){
		Query<User> q = mf.getDatastore().createQuery(User.class);
		mf.getDatastore().save(user);
	}
	
	public User getBackendUserByEmail(String email){
		Query<User> q = mf.getDatastore().createQuery(User.class);
		q.field("email").equal(email);
		q.field("roles").contains(Role.ADMIN.toString());
		return q.get();
		
	}
	
	public User getStudentUserByEmail(String email){
		Query<User> q = mf.getDatastore().createQuery(User.class);
		q.field("email").equal(email);
		q.field("roles").contains(Role.STUDENT.toString());
		return q.get();
		
	}
	
	public User getStudentUserByPhone(String phone){
		Query<User> q = mf.getDatastore().createQuery(User.class);
		q.field("phone").equal(phone);
		q.field("roles").contains(Role.STUDENT.toString());
		return q.get();
		
	}
	
	
	
	public User findById(String id){
		Query<User> q = mf.getDatastore().createQuery(User.class);
		q.field("id").equal(id);
		return q.get();
	}
}
