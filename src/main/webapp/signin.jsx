var React = require('react');
var ReactDOM = require('react-dom');

class Signin extends React.Component{

    constructor(props){
      super(props);
      this.state = {user:{}};
    }
    changeInput(event){
      let name = event.target.name;
      let user = this.state.user;
      user[name] = event.target.value;
      this.setState({user:user});
    }
    saveLogin(event){
      axios.post("/verifysignin?username="+this.state.user.email+"&password="+this.state.user.password,null)
				.then(res =>{
              location.href="/dashboard";
				})
				.catch(function(error){

				});
    }
    render(){
      return(
          <div>
            <div>
              <h4>Login</h4>
              <form>
                <input type name="email" placeholder="email" value={this.state.user.email} onChange={this.changeInput.bind(this)}/>
                <input placeholder="password" name="password" value={this.state.user.password} onChange={this.changeInput.bind(this)}/>
                <button type="button" className="btn" onClick={this.saveLogin.bind(this)}>Login</button>
              </form>
            </div>
          </div>


      );
    }
}

ReactDOM.render(
  <Signin />,document.getElementById('login')
);
