<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="/dist/style.css">
</head>
<body>
<%@include file="./sidebar.jsp"%>
<section class="side-block">
	<header class="header">
		
	</header>
	<div class="content-block">
		<!-- <div class="course-list">
			<div class="individual-course">
				<div>
					<p>G</p>
				</div>
				<div >
					<p>IIPC</p>
				</div>
			</div>
		</div> -->
		<!-- <div class="filter-list" id="filters">
			<select>
				<option>IIPC</option>
				<option>CPMT</option>
			</select>
			<select>
				<option>Maths</option>
				<option>Chemistry</option>
				<option>Physics</option>
			</select>
			<select>
				<option>Tghermodynamics</option>
				<option>Thermodynamics</option>
			</select>
			<select>
				<option>Cardset1</option>
			</select>
		</div> -->
		<div id="testlist">
		</div>

		<div class="sub-list">
			
		</div>
	</div>

</section>

<div class="modal" data-toggle="true" role="dialog" data-state="closed" id = "courseeditmodal">
  
</div>

<div class="modal" data-toggle="true" role="dialog" data-state="closed" id="chaptereditmodal">
  <div class="modal__window auth">
    <button class="modal__close" data-toggle="true">Close</button>
    
    <div class="content-block">
    	
	    <div class="input-blocks">
	    	<h3>Add Patron Chapter</h3>
	    	<input type="" name="" placeholder="Title">
	    	<input type="" name="" placeholder="seqId">
	    </div>
	    <div class="input-blocks">
	    	<h3>Add Chapter</h3>
	    	<select></select>
	    	<input type="" name="" placeholder="add title of subject">
	    	<input type="" name="" placeholder="seqId">
	    </div>
	    <button class="btn">Add subject</button>
	    <p class="err-msg">Some error in adding subject</p>
	</div>	 
  </div>
  <div class="cover"></div>
</div>


<div class="modal" data-toggle="true" role="dialog" data-state="closed" id="qtneditmodal">
  <div class="modal__window auth">
    <button class="modal__close" data-toggle="true">Close</button>
    
    <div class="content-block">
	    <div class="qtn-input-blocks">
	    	<h3>Add Card</h3>
	    	<h4>Question-content</h4>
	    	<div>
	    		<textarea class="textarea" placeholder="content"></textarea>
	    		<input type="" name="" placeholder="time in min" >
			</div>
			<p> Options</p>
			<button>add option</button>
			<div>
				<input type="" name="" placeholder="option id">
				<input type="" name="" placeholder="content">
				<input type="" name="" placeholder=" imageUrl">
				<button>delete</button>
				
			</div>
			<div class="solution-block">
				<p>Solution</p>
				<input type="" name="" placeholder="answer">
				<textarea class="textarea" placeholder="Solution"></textarea>
			</div>
	    </div>
	    <button class="btn">Add subject</button>
	    <p class="err-msg">Some error in adding subject</p>
	</div>	 
  </div>
  <div class="cover"></div>
</div>
<script>

var courses = ${courses};

</script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.17.1/axios.min.js"></script>
<script src="/dist/tests.bundle.js"></script>
</body>
</html>