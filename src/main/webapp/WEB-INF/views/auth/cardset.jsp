<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="approot">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="/dist/style.css">
</head>
<body ng-controller="RootCtrl">
<%@include file="./sidebar.jsp"%>
<section class="side-block">
	<header class="header">
		
	</header>
	<div class="content-block">
		<div class="course-list">
			<!-- <div class="individual-course">
				<div>
					<p>G</p>
				</div>
				<div >
					<p>IIPC</p>
				</div>
			</div> -->
		</div>
		<div class="filter-list">
			<!-- <select>
				<option>IIPC</option>
				<option>CPMT</option>
			</select>
			<select>
				<option>Maths</option>
				<option>Chemistry</option>
				<option>Physics</option>
			</select>
			<select>
				<option>Tghermodynamics</option>
				<option>Thermodynamics</option>
			</select>
			<select>
				<option>Cardset1</option>
			</select> -->
		</div>
		<div class="">
			<table class="card-table">
				<tr class="headers-tables">
					<th>Patron Chapter</th>
					<th>Cardsets</th>
					<th>Card</th>
					<th>Content</th>
				</tr>
				<tr>
					<td valign="top">
						<div class="card-table">
							<button ng-click="editChap()">Add chapter</button>
							<div class="individual-row" ng-repeat="chap in chaptersList"><p ng-click="clickChapter(chap,$index)">{{chap.title}}</p> <img src="#" ng-click="editChap(chap)" /></div>
						</div>
					</td>
					<td valign="top">
						<div>
						<button ng-click="editCardSet()">Add cardset</button>
						 	<div class="individual-row" ng-repeat="cardset in clickedChap.cardSetsList"><p ng-click="clickCardset($index)">{{cardset.title}}</p><img src="#" ng-click="editCardSet(cardset)" /></div>
						</div>
					</td>
					
					<td valign="top" id="cardslist">
						<button type="button" ng-click="editCard()">Add card</button>
						<div class="individual-row" ng-repeat="card in clickedChap.cardSetsList[cardsetIndex].cards" ng-if="card.cardType!='QUESTION'"><p  ng-click="clickCard($index)">{{card.cardItem.title}}</p></div>
					</td>
					
					<td valign="top" id="contentlist">
						  <button ng-click="editCard(clickedChap.cardSetsList[cardsetIndex].cards[cardIndex])">Edit</button>
      					<div ng-bind-html="clickedChap.cardSetsList[cardsetIndex].cards[cardIndex].cardItem.content | unsafe">
					</td>
				</tr>

			</table>
		</div>
		<div class="sub-list">
			
		</div>
	</div>

</section>

<div class="modal" data-toggle="true" role="dialog" data-state="closed" id = "chapeditmodal">
      <div class="modal__window auth">
  <button class="modal__close" data-toggle="true" ng-click="closeModal('chapeditmodal')">Close</button>

  <div class="content-block">

    <div class="input-blocks">
      <h3>Add Patron Chapter</h3>
      <input type="" name="title" placeholder="Title"  ng-model="selPatChap.title" />
      <input type="" name="seqId" placeholder="seqId"  ng-model="selPatChap.seqId" />
    </div>
    <div class="input-blocks">
      <h3>Select Course</h3>
      <select name="courseId" ng-options="course.title for course in courseList" ng-model="selCourse" ng-chn>
        <option value="">Select course</option>
   
      </select>
      <select name="subjectId" ng-model="selSub" ng-options="sub.title for sub in selCourse.subjects">
        <option value="">Select subject</option>
      
      </select>

      <select name="chapterId" ng-options="chap.title for chap in selSub.chapters" ng-model="selChap">
        <option value="">select chapter</option>
      
      </select>

    </div>
    <button class="btn" ng-click="saveChapter()">Save Chapter</button>
    <button ng-click="deleteChapter()">Delete</button>
    <p class="err-msg">Some error in adding subject</p>
</div>
</div>
<div class="cover"></div>
</div>

<div class="modal" data-toggle="true" role="dialog" data-state="closed" id="cardseteditmodal">
   <div>
      <div class="modal__window auth">
  <button class="modal__close" data-toggle="true" ng-click="closeModal('cardseteditmodal')">Close</button>

  <div class="content-block">

    <div class="input-blocks">
      <h3>Add Cardset</h3>
      <input type="" name="title" placeholder="Title" ng-model="selCardset.title" />
      <input type="" name="seqId" placeholder="seqId" ng-model="selCardset.seqId" />
    </div>
    <button class="btn" ng-click="saveCardset()" >Add Cardset</button>
     <button ng-click="deleteCardset()">Delete</button>
    <p class="err-msg">Some error in adding subject</p>
</div>
</div>
<div class="cover"></div>
</div>
</div>


<div class="modal" data-toggle="true" role="dialog" data-state="closed" id="cardeditmodal">
   <div>
      <div class="modal__window auth">
  <button class="modal__close" data-toggle="true" ng-click="closeModal('cardeditmodal')">Close</button>

  <div class="content-block">

    <div class="input-blocks">
      <h3>Add Card</h3>
      <input type="" name="title" placeholder="Title" ng-model="selCard.cardItem.title" />
      <input type="" name="seqId" placeholder="seqId" ng-model="selCard.cardItem.seqId" />
      <textarea rows="" cols="" ng-model="selCard.cardItem.content"></textarea>
    </div>
    <button class="btn" ng-click="saveCard()" >Add Card</button>
    <button ng-click="deleteCard()">Delete</button>
    <p class="err-msg">Some error in adding subject</p>
</div>
</div>
<div class="cover"></div>
</div>

</div>
<script>
var courseList  = ${courseList};
</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.6/angular.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.17.1/axios.min.js"></script>
<!-- <script src="/dist/cardset.bundle.js"></script> -->

<script>
var rootapp = angular.module('approot',[
                                    	'appCtrls'
                                    ]);

 angular.module('appCtrls',[])


 .controller('RootCtrl',function($scope,$rootScope,$window,$http,$timeout, $q) {
	 $scope.courseList = window.courseList;
	 
	 $scope.getChapters = function(){
		 
		 var url = "/sp/getchapters";
		 $http({url:url,methos:"GET",
				dataType:"json",
				headers:{"Content-Type":"application/json"}
			 }).
			 success(function(data,status,headers,config){
				 $scope.chaptersList = data.data;
				 console.log($scope.chaptersList);
			 }).
			 error(function(data,status,headers,config){
					
			 });
			 /* axios.get("/sp/getchapters")
			 .then(function(response){
			   
			 }); */
			 /* .catch(function(error){

			 }); */
	 }
	 
	 $scope.getChapters();
	 $scope.clickChapter = function(chap){
		 
		 $scope.clickedChap = angular.copy(chap);
		 /* $http({url:url,method:"GET",
			 dataType:"json",
			 headers:{"Content-Type":"application/json"}
		 }).
		 success(function(data,status,headers,config){
				 
		 }).
		 error(function(data,status,headers,config){
			 
		 }); */
	 }
	 
	 $scope.clickCardset = function(index){
		$scope.cardsetIndex = index;
		 var url ="/sp/getcards/"+ $scope.clickedChap.cardSetsList[index].id;
		 $http({url:url,method:"GET",
		 dataType:"json",
		 headers:{"Content-Type":"application/json"}
		 }).
		 success(function(data,status,headers,config){
			 
			 $scope.clickedChap.cardSetsList[$scope.cardsetIndex].cards = data.data;
			 console.log($scope.clickedChap.cardSetsList[$scope.cardsetIndex].cards);
		 }).
		 error(function(data,status,headers,config){
			 
		 });
	 }
	 
	 $scope.clickCard = function(index){
		 $scope.cardIndex = index;
	 }
	 
	 $scope.editChap = function(chap){
		 document.getElementById("chapeditmodal").setAttribute("data-state","open");
		 if(chap==null)
			 chap = {};
		 
		 $scope.selPatChap  = angular.copy(chap);
	 }
	 
	 $scope.editCardSet = function(chap){
		 console.log("sss");
		 document.getElementById("cardseteditmodal").setAttribute("data-state","open");
		 if(chap==null)
			 chap = {};
		 
		 $scope.selCardset  = angular.copy(chap);
	 }
	 
	 $scope.editCard = function(chap){
		 console.log("sss");
		 
		 document.getElementById("cardeditmodal").setAttribute("data-state","open");
		 if(chap==null){
			 chap = {};
			 chap.cardItem = {};
		 }
		 
		 $scope.selCard  = chap;
		 
	 }
	 
	 
	 
	 $scope.saveCardset = function(){
		 var url = "/sp/savecardset/"+$scope.clickedChap.id;
		 
		 $http({method:"POST",url:url,
		dataType:"json",
		data:$scope.selCardset
		 }).
		 success(function(data,status,headers,config){
			 if($scope.selCardset.id!=null){
				 for(var i=0; i< $scope.clickedChap.cardSetsList.length;i++){
					 if($scope.clickedChap.cardSetsList[i].id==$scope.selCardset.id){
						 $scope.clickedChap.cardSetsList[i].title = data.data.title;
						 $scope.clickedChap.cardSetsList[i].seqId = data.data.seqId;
					 }
				 }
			 }
			 else{
				 if($scope.clickedChap.cardSetsList==null)
					 $scope.clickedChap.cardSetsList=[];
				 $scope.clickedChap.cardSetsList.push(data.data);
			 }
				 
		 }).
		 error(function(data,stattus,headers,config){
			 
		 });
	 }
	 
	 
	 $scope.saveCard = function(){
		 
		 console.log($scope.selCard);
	
		 
		 if($scope.selCard.id!=null)
		      url = '/sp/savetheorycard/'+$scope.clickedChap.subjectId+'/'+$scope.clickedChap.chapterId+'/'+$scope.clickedChap.id+'/'+$scope.clickedChap.cardSetsList[$scope.cardsetIndex].id+'?cardId='+$scope.selCard.id;
		    else
		    	url = '/sp/savetheorycard/'+$scope.clickedChap.subjectId+'/'+$scope.clickedChap.chapterId+'/'+$scope.clickedChap.id+'/'+$scope.clickedChap.cardSetsList[$scope.cardsetIndex].id;
		
		 
		 $http({method:"POST",url:url,
		dataType:"json",
		data:$scope.selCard.cardItem
		 }).
		 success(function(data,status,headers,config){
			 if($scope.selCard.id!=null){
				 for(var i=0; i< $scope.clickedChap.cardSetsList.length;i++){
					
					 if($scope.clickedChap.cardSetsList[$scope.cardsetIndex].cards!=null){
						 for(var j=0; j< $scope.clickedChap.cardSetsList[$scope.cardsetIndex].cards.length;j++){
							 if($scope.clickedChap.cardSetsList[$scope.cardsetIndex].cards[j].id == $scope.selCard.id){
								 $scope.clickedChap.cardSetsList[$scope.cardsetIndex].cards[j].$scope.selCard;
							 }
						 }
					 }
					 
				 }
			 }
			 else{
				 if($scope.clickedChap.cardSetsList[$scope.cardsetIndex].cards==null)
					 $scope.clickedChap.cardSetsList[$scope.cardsetIndex].cards=[];
				 $scope.clickedChap.cardSetsList[$scope.cardsetIndex].cards.push(data.data);
			 }
				 
		 }).
		 error(function(data,stattus,headers,config){
			 
		 });
	 }
	 
	 
	 $scope.closeModal = function(id){
		 document.getElementById(id).setAttribute("data-state","closed");
	 }
	 
	 $scope.deleteChapter = function(){
		 
		 var url = "/sp/deletepatronchap/"+$scope.selPatChap.id;
		 $http({url:url,method:"POST",
				dataType:"json",
			 }).
			 success(function(data,status,headers,config){
				 location.reload();
			 }).
			 error(function(data,status,headers,config){
				 console.log("error");
				 alert("cardsets exists delete them");
			 });
	 }
	 
		$scope.deleteCardset = function(){
		 console.log("delete cardset");
		 console.log($scope.selCardset);
		
		 var url = "/sp/deletecardset/"+$scope.selCardset.id+"/"+$scope.clickedChap.id;
		 $http({url:url,method:"POST",
				dataType:"json",
			 }).
			 success(function(data,status,headers,config){
				 location.reload();
			 }).
			 error(function(data,status,headers,config){
				 console.log("error");
				 alert("cards exists delete them");
			 });
	 }
		
		$scope.deleteCard = function(){
			 
			 var url = "/sp/deletecard/"+$scope.selCard.id+"/"+$scope.clickedChap.id;
			 $http({url:url,method:"POST",
					dataType:"json",
				 }).
				 success(function(data,status,headers,config){
					 
					 location.reload();
				 }).
				 error(function(data,status,headers,config){
					 
				 });
		 }
	 $scope.saveChapter = function(){
		var url = "/sp/savepatronchapter";
		$scope.selPatChap.chapterId = $scope.selChap.id;
		$scope.selPatChap.subjectId = $scope.selSub.id;
		$scope.selPatChap.coursetId = $scope.selCourse.id;
		 $http({url:url,method:"POST",
			dataType:"json",
			data:$scope.selPatChap
		 }).
		 success(function(data,status,headers,config){
			 if($scope.selPatChap.id==null){
				 if($scope.chaptersList==null)
					 $scope.chaptersList=[];
				 
				 $scope.chaptersList.push(data.data);
			 }
		 }).
		 error(function(data,status,headers,config){
			 
		 });
		 
	 }
	 
 })
 .filter("unsafe",function($sce){
	return function(val){
		return $sce.trustAsHtml(val);
	}
});

</script>
</body>
</html>