<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="approot">
<head >
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="/dist/style.css">
</head>
<body ng-controller="RootCtrl">
<%@include file="./sidebar.jsp"%>


<section class="side-block">
	<header class="header">
		
	</header>
	<div class="content-block">
		<select ng-model="selChap" ng-options="chap.title for chap in chapters">
          <option value="">Select Chapter</option>
        
		</select>
	
		<select ng-model="selCardset" ng-change="getCards()" ng-options="cardset.title for cardset in selChap.cardSetsList">
			<option value="">select cardset</option>
		</select>
		
		<select ng-model="selCard" ng-change="getQtns()" ng-options="card.cardItem.title for card in cards">
			<option value="">select card</option>
		
		</select>
		
		<button ng-click="editQtn()">Add question</button>
		<div id="qtnlist">
			<div class="question" ng-repeat="qtn in qList">
			      <h4>Question</h4>
			      <button ng-click="editQtn(qtn,$index)">Edit qtn</button>
			      <p ng-bind-html="qtn.cardItem.content| unsafe"></p>
			      <div class="options">
			      <h4>Options</h4>
			      
			        <div ng-repeat="option in qtn.cardItem.options">
			          <p><span class="id">{{option.id}}</span>{{option.content}}</p>
			        </div>
			    
			      </div>
			      <h4>Solution</h4>
			      <h5>Answer- {{qtn.cardItem.solution.answer}}</h5>
			      <p>{{qtn.cardItem.solution.solution}}</p>
			  </div>
		
		</div>
	</div>

</section>


<div class="modal" data-toggle="true" role="dialog" data-state="closed" id="qtneditmodal">
  <div>
        <div class="modal__window auth">
          <button class="modal__close" data-toggle="true" ng-click="closeModal('qtneditmodal')">Close</button>
          <div class="content-block">
            <div class="qtn-input-blocks">
              <h3>Add Question</h3>
              <h4>Question-content</h4>
              <select name="difficulty" ng-change="changeQuestion()" ng-model="clickedQ.difficulty" >
                <option value="">Select Difficulty</option>
                <option value="EASY">Easy</option>
                <option value="MEDIUM">Medium</option>
                <option value="HARD">Hard</option>
              </select>

              <select name="type"  ng-model="clickedQ.type">
                <option value="">Select question type</option>
                <option value="SINGLE">Single Choice</option>
                <option value="MULTIPLE">Multiple Choice</option>
                <option value="BLANK">Blank</option>
                <option value="MATRIX">Matrix</option>
              </select>
              <div>
                <textarea class="textarea" placeholder="content" name="content" ng-model="clickedQ.content"></textarea>
                <input placeholder="paste image url" name="imageUrl" ng-model="clickedQ.imageUrl"/>
              </div>
              <p> Options</p>
              <button ng-click="addOption()">add option</button>
              
              <div ng-repeat="option in clickedQ.options">
                <input name="id" placeholder="option id" ng-model="option.id" />
                <input name="content" ng-model="option.content" placeholder="content" />
                <input name="imageUrl" ng-model="option.imageUrl" placeholder=" imageUrl" />
                <button ng-click="deleteOption($index)">delete</button>
              </div>
             
              <div class="solution-block">
                <p>Solution</p>
                <input placeholder="answer" name="answer" ng-model= "clickedQ.solution.answer" >
                <textarea class="textarea" placeholder="Solution" name="solution" ng-model="clickedQ.solution.solution"> </textarea>
              </div>
            </div>
            <button class="btn" ng-click="saveQtn()">Add Question</button>
            <button class="btn" ng-click="deleteQtn()">Delete Question</button>
            <p class="err-msg">Some error in adding subject</p>
          </div>

        </div>
        <div class="cover"></div>
      </div>
</div>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.6/angular.min.js"></script>
<script>
var rootapp = angular.module('approot',[
                                    	'appCtrls'
                                    ]);

 angular.module('appCtrls',[])
 .controller('RootCtrl',function($scope,$rootScope,$window,$http,$timeout, $q) {
	console.log("shabri");
	 $scope.getChapters = function(){
		 $http({method:"GET",url:"/sp/getchapters",
			 dataType:"json",
		 }).
		 success(function(data,status,headers,config){
			 $scope.chapters = data.data;
		 }).
		 error(function(data,status,headers,config){
			 
		 });
	 }
	 
	$scope.getChapters(); 
	
	$scope.getCards = function(){
		var url = "/sp/getcards/"+$scope.selCardset.id;
		$http({method:"GET",url:url,
			dataType:"json",	
		}).
		success(function(data,status,headers,config){
			$scope.cards = data.data;
		}).
		error(function(data,status,headers,config){
			
		});
		
	}
	
	$scope.editQtn = function(qtn,index){
		console.log(qtn);
		if(qtn==null){
			console.log("intot hsi");
			$scope.clickedQ = {}
			$scope.clickedQ.solution = {};
		}
		else{
			console.log("not this");
			$scope.clickedQ = $scope.qList[index].cardItem;
		
			$scope.clickedIndex = index;
		}
		
		console.log($scope.clickedQ);
		
		document.getElementById("qtneditmodal").setAttribute("data-state","open");
	}
	
	$scope.closeModal = function(){
		document.getElementById("qtneditmodal").setAttribute("data-state","closed");
		$scope.clickedQ = {};
	}
	
	
	$scope.saveQtn = function(){
		
		
		var url = '/sp/saveqtncard/'+$scope.selChap.subjectId+'/'+$scope.selChap.chapterId+'/'+$scope.selChap.id+'/'+$scope.selCardset.id+'/'+$scope.selCard.id;
		if($scope.clickedQ.id!=null)
			url = url+"?cardId="+$scope.clickedQ.id;
		
		$http({method:"POST",url:url,
			dataType:"json",
			data:$scope.clickedQ
		}).
		success(function(data,status,headers,config){
			if($scope.clickedQ.id==null){
				$scope.qList.push(data.data);
			}
			else{
				$scope.qList[$scope.clickedIndex] = data.data;
			}
			
			$scope.closeModal();
		}).
		error(function(data,status,headers,config){
			
		});
	}
	
	$scope.addOption = function(){
		if($scope.clickedQ.options==null)
			$scope.clickedQ.options = [];
		var option ={};
		$scope.clickedQ.options.push(option);
		
	}
	
	$scope.deleteOption = function(index){
		
		$scope.clickedQ.options.splice(index,1);
		
	}
	
	
	$scope.deleteQtn = function(){
		 var url = "/sp/deleteqtncard/"+$scope.qList[$scope.clickedIndex].id;
		 $http({method:"POST",url:url,
	
		 }).
		 success(function(data,status,headers,config){
			 $scope.qList.splice($scope.clickedIndex,1);
			 $scope.closeModal();
		 }).
		 error(function(data,status,headers,config){
			 
		 });
	 }
	$scope.getQtns = function(){
		var url = "/sp/getqtncards/"+$scope.selCard.id;
		$http({method:"GET",url:url,
		dataType:"json"	
		}).
		success(function(data,status,headers,config){
			$scope.qList = data.data;
		}).
		error(function(data,status,headers,config){
			
		});
	}
	 
	 
 })
 .filter("unsafe",function($sce){
	return function(val){
		return $sce.trustAsHtml(val);
	}
});
 
 
</script>
</body>
</html>