<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="/dist/style.css">
</head>
<body>
<%@include file="./sidebar.jsp"%>
<section class="side-block">
	<header class="header">
		
	</header>
	<div class="content-block">
		<div class="course-list">
			<div class="individual-course">
				<div>
					<p>G</p>
				</div>
				<div >
					<p>IIPC</p>
				</div>
			</div>
		</div>
		<div id="tableblock">
		
		</div>
		<div class="sub-list">
			
		</div>
	</div>

</section>

<div class="modal" data-toggle="true" role="dialog" data-state="closed" id = "courseeditmodal">
  
</div>

<div class="modal" data-toggle="true" role="dialog" data-state="closed">
  <div class="modal__window auth">
    <button class="modal__close" data-toggle="true">Close</button>
    
    <div class="content-block">
    	<div>
    		<input type="radio" name="type">Test<br>
    		<input type="radio" name="type">Chapter<br>
    	</div>
	    <div class="input-blocks">
	    	<h3>Add Test</h3>
	    	<input type="" name="" placeholder="Title">
	    	<input type="" name="" placeholder="seqId">
	    	<input type="" name="" placeholder="time in min">
	    	<input type="" name="" placeholder="questions">
	    	<input type="" placeholder="marks">
	    </div>
	    <div class="input-blocks">
	    	<h3>Add Chapter</h3>
	    	<input type="" name="" placeholder="add title of subject">
	    	<input type="" name="" placeholder="seqId">
	    </div>
	    <button class="btn">Add subject</button>
	    <p class="err-msg">Some error in adding subject</p>
	</div>	 
  </div>
  <div class="cover"></div>
</div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.17.1/axios.min.js"></script>
<script src="/dist/dashboard.bundle.js"></script>
</body>
</html>