var React = require('react');
var ReactDOM = require('react-dom');




class QuestionList extends React.Component{
  constructor(props){
    super(props);
  }

  addQuestion(card,question){
    document.getElementById("qtneditmodal").setAttribute("data-state","open");
    ReactDOM.render(
      <AddQuestion  question = {question} ts={this.props.ts}/>,
      document.getElementById("qtneditmodal")
    );
  }
  render(){

    return(

        <div>
        <button onClick={this.addQuestion.bind(this)} >Add question</button>
        {this.props.list.map((question,index) =>
          <Question q = {question}/>
        )}
        </div>
    );
  }
}

class AddQuestion extends React.Component{
  constructor(props){
    super(props);
    var q = this.props.question;

    if(q==null){
      q = {};
    }
    if(q.solution==null)
    q.solution = {};
    if(q.options==null)
    q.options = [];
    this.state={q:q};

  }

  addOption(){
    let q = this.state.q;
    if(q.options==null)
      q.options=[];
    let option = {};
    q.options.push(option);
    this.setState({q:q});
  }
  deleteOption(event,index){
    let q = this.state.q;
    q.options.splice(index,1);
    this.setState({q:q});
  }
  changeOption(index,event){
    console.log(event);
    let q = this.state.q;
    console.log(q);
    q.options[index][event.target.name]= event.target.value;

    this.setState({q:q});
    console.log(this.state.q);
  }

  changeSolution(event){
    let q = this.state.q;
    q.solution[event.target.name]= event.target.value;
    this.setState({q:q});
  }


  changeQuestion(event){
    let q = this.state.q;
    q[event.target.name]= event.target.value;
    this.setState({q:q});
  }
  saveQuestion(){
    //let q = this.state.q;
    //q.solution.answersList = q.solution.answer.split(',');
    var url;

    url = '/ts/saveqtn/'+this.props.ts.id;

    axios.post(url,this.state.q)
    .then(res=> {

    })
    .catch(function(error){

    });
  }
  render(){
    return(
      <div>
        <div className="modal__window auth">
          <button className="modal__close" data-toggle="true">Close</button>
          <div className="content-block">
            <div className="qtn-input-blocks">
              <h3>Add Card</h3>
              <h4>Question-content</h4>
              <div>
                <textarea className="textarea" placeholder="content" name="content" value={this.state.q.content} onChange={this.changeQuestion.bind(this)}/>
                <input placeholder="time in min" onChange={this.changeQuestion.bind(this)} name="imageUrl" value={this.state.q.imageUrl}/>
              </div>
              <p> Options</p>
              <button onClick={this.addOption.bind(this)}>add option</button>
              {this.state.q.options!=null && this.state.q.options.map((option,index) =>
              <div key={index}>
                <input name="id" value={option.id} placeholder="option id" onChange={this.changeOption.bind(this,index)} />
                <input name="content" value={option.content} placeholder="content" onChange={this.changeOption.bind(this,index)} />
                <input name="imageUrl" value={option.imageUrl} placeholder=" imageUrl" onChange={this.changeOption.bind(this,index)} />
                <button onClick={this.deleteOption.bind(this,index)}>delete</button>
              </div>
              )}
              <div className="solution-block">
                <p>Solution</p>
                <input placeholder="answer" name="answer" value= {this.state.q.solution.answer} onChange= {this.changeSolution.bind(this)}/>
                <textarea className="textarea" placeholder="Solution" name="solution" value={this.state.q.solution.solution} onChange= {this.changeSolution.bind(this)}/>
              </div>
            </div>
            <button className="btn" onClick={this.saveQuestion.bind(this)}>Add subject</button>
            <p className="err-msg">Some error in adding subject</p>
          </div>

        </div>
        <div className="cover" />
      </div>

    );
  }
}


class Question extends React.Component{
  constructor(props){
    super(props);
  }
  render(){
    return(
      <div>
        <h4>Question</h4>
        <p>{this.props.q.content}</p>
        <h4>Options</h4>
        {this.props.q.options.map((option,index) =>
          <div>
            <p><span>{option.id}</span>{option.content}</p>
          </div>
        )}
        <h4>Solution</h4>
        <h5>Answer- {this.props.q.solution.answer}</h5>
        <p>{this.props.q.solution.solution}</p>
      </div>
    );
  }
}
if(window.ts.questionIds!=null){
axios.get("/ts/getqtns/"+window.ts.id)
.then(res=>{
  ReactDOM.render(
    <QuestionList  list = {res.data.data} ts = {window.ts}/>,
    document.getElementById("filters")
  );
})
.catch(function(error){

});
}
else{
  var q = []
  ReactDOM.render(
    <QuestionList  list = {q} ts = {window.ts}/>,
    document.getElementById("filters")
  );
}
