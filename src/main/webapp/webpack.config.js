
const path = require('path');
module.exports = {
		entry:{
			a:"./test.jsx",
			b:"./test2.jsx",
			c:"./signin.jsx",
			dashboard:"./dashboard.jsx",
			cardset:"./cardset.jsx",
			addqtn:"./addqtn.jsx",
			tests:"./tests.jsx",
			testqtn:"./testqtn.jsx"
		},
		output:{
			filename: "./[name].bundle.js"
		},
		module: {
			rules: [
			          {
			            test: /\.jsx?/, // include .js file
			            exclude: /node_modules/, // exclude any and all files in the node_modules folder
			            use: [
			              {
			                loader: "babel-loader" ,
			                options: {
			                	"presets": [
			                	            ["es2016"],
			                	            "react"
			                	          ],
			                	          "plugins": [
			                	            "babel-plugin-transform-class-properties"
			                	          ]
			                 }
			              }
			            ]
			          }
			        ]
	     }
}
