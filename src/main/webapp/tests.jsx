var React = require('react');
var ReactDOM = require('react-dom');

class TestsList extends React.Component{
  constructor(props){
    super(props);
    var tests = this.props.tests;
    if(tests==null)
      tests =[];
    this.state={tests:tests};
  }


  addTest(test){
      document.getElementById("courseeditmodal").setAttribute("data-state","open");
      ReactDOM.render(
        <AddTest  test = {test} courses={this.props.courses}/>,
        document.getElementById("courseeditmodal")
      );
  }

  

  render(){
    return(
      <div>
        <button onClick={this.addTest.bind(this)}>Add Test</button>
        <div className="testslist">
            {this.state.tests.map((test,index) =>
            <div className="individual-test">
                <a href={'/tests/get/'+test.id}><p>{test.title}</p></a>
            </div>
            )}
        </div>
      </div>
    );
  }
}

class AddTest extends React.Component{
  constructor(props){
    super(props);
    var test = this.props.test;
    if(test==null)
      test = {};
    let item = {};
    item.test = test;
    item.selChapsIds=[];
    this.state={item:item};
  }

  getChaters(courseId){
    var courses = this.props.courses;
    var objectsList = [];
    for(var i=0 ; i< courses.length;i++){
      if(courses[i].id == courseId){
        for(var k =0; k< courses[i].subjects.length;k++){
          if(this.state.item.test.testType=='SUBJECT')
          objectsList.push(courses[i].subjects[k]);
          for(var j=0; j< courses[i].subjects[k].chapters.length;j++){
            if(this.state.item.test.testType=='CHAPTER')
              objectsList.push(courses[i].subjects[k].chapters[j]);
          }
        }
      }
    }

    var item = this.state.item;
    if(item.objectsList==null)
      item.objectsList = [];

    item.objectsList = objectsList;
    this.setState({item:item});
    console.log(this.state);
  }
  changeInput(event){
    var item = this.state.item;
    item.test[event.target.name] = event.target.value;
    this.setState({item:item});

    console.log(this.state.item.test.courseId);
  }

  changeType(event){
    var item = this.state.item;
    item.test[event.target.name] = event.target.value;
    item.selChapsIds=[];
    this.setState({item:item});
    console.log(this.state.item.test.courseId);
  }


  changeCourse(event){
    var item = this.state.item;
    item.test[event.target.name] = event.target.value;
    this.setState({item:item});
    console.log(this.state.item.test.courseId);

    this.getChaters(event.target.value);
  }

  selChapters(event){
    var item = this.state.item;
    if(item.selChapsIds==null)
      item.selChapsIds = [];
    let chaps = this.state.item.objectsList;
    for(var i=0; i< chaps.length;i++){

      if(chaps[i].id == event.target.value){
        var index = item.selChapsIds.indexOf(event.target.value);
        if(index > -1)
          item.selChapsIds.splice(index,1);
        else
          item.selChapsIds.push(event.target.value);
      }
    }
    this.setState({item:item});
    console.log(this.state);

  }
  saveTest(){
    let ts = this.state.item.test;
    let objectIds = this.state.item.selChapsIds;
    for(var i=0 ; i< courses.length;i++){
      if(courses[i].id == ts.courseId){
        for(var k =0; k< courses[i].subjects.length;k++){
          if(ts.testType=='SUBJECT'){
            if(this.state.item.selChapsIds.indexOf(courses[i].subjects[k].id)>-1){
              if(ts.subjects==null)
                ts.subjects=[];
              ts.subjects.push(courses[i].subjects[k]);
            }
          }
          for(var j=0; j< courses[i].subjects[k].chapters.length;j++){
            if(ts.testType =='CHAPTER'){
              if(this.state.item.selChapsIds.indexOf(courses[i].subjects[k].chapters[j].id)>-1){
                if(ts.chapters==null)
                  ts.chapters=[];
                ts.chapters.push(courses[i].subjects[k].chapters[j]);
              }
            }
          }
        }
      }
    }
    console.log(ts);
    axios.post('/ts/create',ts)
      .then(res =>{

      })
      .catch(function(error){

      });
  }

  render(){
    return(
      <div>
        <div className="modal__window auth">
          <button className="modal__close" data-toggle="true">Close</button>
          <div className="content-block">
            <div className="input-blocks">
              <h3> Add Test</h3>
              <input name="title" placeholder="Title" onChange={this.changeInput.bind(this)}/>
              <input name="seqId" placeholder="seqId" onChange={this.changeInput.bind(this)}/>
              <input name="duration" placeholder="time in min" onChange={this.changeInput.bind(this)}/>
              <input name="totalMarks" placeholder="marks" onChange={this.changeInput.bind(this)}/>
              <select name="testType" onChange={this.changeType.bind(this)}>
                <option value="CHAPTER">chapter</option>
                <option value="SUBJECT">subject</option>
              </select>
              <select name="courseId" onChange={this.changeCourse.bind(this)} >
                <option value="">select course</option>
                {this.props.courses.map((course,index) =>
                    <option value={course.id}>{course.title}</option>
                )}
              </select>

              {this.state.item.test.courseId != null && this.state.item.objectsList.map((chap,index) =>
                  <div>
                  <input type="checkbox" value={chap.id} checked={this.state.item.selChapsIds.indexOf(chap.id)>-1} onChange={this.selChapters.bind(this)} />
                    {chap.title}
                  </div>
            )}


            </div>

            <button className="btn" onClick={this.saveTest.bind(this)}>Add subject</button>
            <p className="err-msg">Some error in adding subject</p>
          </div>

        </div>
        <div className="cover" />
      </div>


    );
  }
}

axios.get('/gettests')
  .then(res=>{
    ReactDOM.render(
      <TestsList  courses = {window.courses} tests ={res.data.data} />,
      document.getElementById("testlist")
    );
  }).
  error(function(error){

  });
