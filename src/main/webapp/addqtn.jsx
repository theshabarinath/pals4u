var React = require('react');
var ReactDOM = require('react-dom');

class SelectBar extends React.Component{
  constructor(props){
    super(props);
    let cardState = {};
    cardState.selectedChapIndex=-1;
    this.state={cardState:cardState};

  }

  changeChapter(event){
    var cardState = this.state.cardState;
    cardState.selectedChapIndex = event.target.value;
    cardState.selChap = this.props.chapters[cardState.selectedChapIndex];
    this.setState({cardState:cardState});
    console.log(this.state.cardState.selectedChapIndex);

  }
  changeCardset(event){
    var cardState = this.state.cardState;
    let cardsets = cardState.selChap.cardSetsList;
    for(var i=0; i< cardsets.length;i++){
      if(cardsets[i].id== event.target.value)
        cardState.selCardset = cardsets[i];
    }


    axios.get("/sp/getcards/"+event.target.value)
    .then(res=>{

      cardState.cards = res.data.data;
      console.log(cardState);
      this.setState({cardState:cardState});
    }).
    catch(function(error){

    });
  }

  changeCard(event){
    var cardState = this.state.cardState;
    var cards = cardState.cards;
    for(var i=0; i< cards.length;i++){
      if(cards[i].id == event.target.value)
        cardState.selCard = cards[i];
    }

    this.setState({cardState:cardState});

    axios.get("/sp/getqtncards/"+event.target.value)
    .then(res=>{

      ReactDOM.render(
        <QuestionList list = {res.data.data} chap={this.state.cardState.selChap} cardset={this.state.cardState.selCardset}  parentCard={this.state.cardState.selCard}/>,
        document.getElementById('qtnlist')
      );
    }).
    catch(function(error){

    });
  }
  render(){
    console.log("caleed");
    return(
      <div>
        <select onChange={this.changeChapter.bind(this)} name="index">
          <option value="">Select Chapter</option>
          {this.props.chapters.map((chap,index)=>
              <option value={index}>{chap.title}</option>
          )}
			   </select>
         {this.state.cardState.selectedChapIndex>-1 && this.props.chapters[this.state.cardState.selectedChapIndex].cardSetsList!=null &&
         <select onChange={this.changeCardset.bind(this)} name="index">
           <option value="">select cardset</option>
           {this.props.chapters[this.state.cardState.selectedChapIndex].cardSetsList.map((card,index)=>
              <option value={card.id} >{card.title}</option>
           )}
         </select>
        }

         {this.state.cardState.cards!=null &&
         <select onChange={this.changeCard.bind(this)}>
           <option value="">select card</option>
            {this.state.cardState.cards.map((card,index) =>
              <option value={card.id}>{card.cardItem.title}</option>
            )}
         </select>
        }
      </div>
    );
  }
}


class QuestionList extends React.Component{
  constructor(props){
    super(props);
  }

  addQuestion(card,question){
    document.getElementById("qtneditmodal").setAttribute("data-state","open");
    ReactDOM.render(
      <AddQuestion  question = {question} card={card} chap = {this.props.chap} cardset={this.props.cardset} parentCard ={this.props.parentCard} />,
      document.getElementById("qtneditmodal")
    );
  }
  
  render(){

    return(
        <div>
        <button onClick={this.addQuestion.bind(this)} >Add question</button>
        {this.props.list.map((question,index) =>
          <Question card = {question} editQtn = {this.addQuestion.bind(this)}/>
        )}
        </div>
    );
  }
}


class AddQuestion extends React.Component{
  constructor(props){
    super(props);
    var q = this.props.question;
    var card = this.props.card;

    if(q==null){
      q = {};
    }
    if(q.solution==null)
    q.solution = {};
    if(q.options==null)
    q.options = [];
    this.state={q:q};

  }

  addOption(){
    let q = this.state.q;
    if(q.options==null)
      q.options=[];
    let option = {};
    q.options.push(option);
    this.setState({q:q});
  }
  deleteOption(event,index){
    let q = this.state.q;
    q.options.splice(index,1);
    this.setState({q:q});
  }
  changeOption(index,event){
    console.log(event);
    let q = this.state.q;
    console.log(q);
    q.options[index][event.target.name]= event.target.value;

    this.setState({q:q});
    console.log(this.state.q);
  }

  changeSolution(event){
    let q = this.state.q;
    q.solution[event.target.name]= event.target.value;
    this.setState({q:q});
  }


  changeQuestion(event){
    let q = this.state.q;
    q[event.target.name]= event.target.value;
    this.setState({q:q});
  }
  saveQuestion(){
    //let q = this.state.q;
    //q.solution.answersList = q.solution.answer.split(',');
    var url;
    console.log(this.props.card);
    if(this.card==null || this.props.card===undefined)
    url = '/sp/saveqtncard/'+this.props.chap.subjectId+'/'+this.props.chap.chapterId+'/'+this.props.chap.id+'/'+this.props.cardset.id+'/'+this.props.parentCard.id;
    else
    url = '/sp/saveqtncard/'+this.props.chap.subjectId+'/'+this.props.chap.chapterId+'/'+this.props.chap.id+'/'+this.props.cardset.id+'/'+this.props.parentCard.id+'?cardId='+this.props.card.id;
    
    axios.post(url,this.state.q)
    .then(res=> {
        location.reload();
    })
    .catch(function(error){

    });
  }
  closeCard(){
    ReactDOM.unmountComponentAtNode(document.getElementById('qtneditmodal'));
    document.getElementById("qtneditmodal").setAttribute("data-state","closed");
  }
  render(){
    return(
      <div>
        <div className="modal__window auth">
          <button className="modal__close" data-toggle="true" onClick={this.closeCard.bind(this)}>Close</button>
          <div className="content-block">
            <div className="qtn-input-blocks">
              <h3>Add Question</h3>
              <h4>Question-content</h4>
              <select name="difficulty" onChange={this.changeQuestion.bind(this)} value={this.state.q.difficulty} >
                <option value="">Select Difficulty</option>
                <option value="EASY">Easy</option>
                <option value="MEDIUM">Medium</option>
                <option value="HARD">Hard</option>
              </select>

              <select name="type" onChange={this.changeQuestion.bind(this)} value={this.state.q.type}>
                <option value="">Select question type</option>
                <option value="SINGLE">Single Choice</option>
                <option value="MULTIPLE">Multiple Choice</option>
                <option value="BLANK">Blank</option>
                <option value="MATRIX">Matrix</option>
              </select>
              <div>
                <textarea className="textarea" placeholder="content" name="content" value={this.state.q.content} onChange={this.changeQuestion.bind(this)}/>
                <input placeholder="paste image url" onChange={this.changeQuestion.bind(this)} name="imageUrl" value={this.state.q.imageUrl}/>
              </div>
              <p> Options</p>
              <button onClick={this.addOption.bind(this)}>add option</button>
              {this.state.q.options!=null && this.state.q.options.map((option,index) =>
              <div key={index}>
                <input name="id" value={option.id} placeholder="option id" onChange={this.changeOption.bind(this,index)} />
                <input name="content" value={option.content} placeholder="content" onChange={this.changeOption.bind(this,index)} />
                <input name="imageUrl" value={option.imageUrl} placeholder=" imageUrl" onChange={this.changeOption.bind(this,index)} />
                <button onClick={this.deleteOption.bind(this,index)}>delete</button>
              </div>
              )}
              <div className="solution-block">
                <p>Solution</p>
                <input placeholder="answer" name="answer" value= {this.state.q.solution.answer} onChange= {this.changeSolution.bind(this)}/>
                <textarea className="textarea" placeholder="Solution" name="solution" value={this.state.q.solution.solution} onChange= {this.changeSolution.bind(this)}/>
              </div>
            </div>
            <button className="btn" onClick={this.saveQuestion.bind(this)}>Add Question</button>
            <p className="err-msg">Some error in adding subject</p>
          </div>

        </div>
        <div className="cover" />
      </div>

    );
  }
}

class Question extends React.Component{
  constructor(props){
    super(props);
  }

  editQtn(){
    this.props.editQtn(this.props.card,this.props.card.cardItem);
  }
  
  render(){
    return(
      <div className="question">
        <h4>Question</h4>
        <button onClick={this.editQtn.bind(this)}>Edit qtn</button>
        <p>{this.props.card.cardItem.content}</p>
        <div className="options">
        <h4>Options</h4>
        {this.props.card.cardItem.options!=null && this.props.card.cardItem.options.map((option,index) =>
          <div>
            <p><span className="id">{option.id}</span>{option.content}</p>
          </div>
        )}
        </div>
        <h4>Solution</h4>
        <h5>Answer- {this.props.card.cardItem.solution.answer}</h5>
        <p>{this.props.card.cardItem.solution.solution}</p>
      </div>
    );
  }
}

axios.get("/sp/getchapters")
.then(res=>{
  ReactDOM.render(
    <SelectBar  chapters = {res.data.data}/>,
    document.getElementById("filters")
  );
})
.catch(function(error){

});
