
var React = require('react');
var ReactDOM = require('react-dom');



class CourseSkeleton extends React.Component{
		constructor(props){
			super(props);
			this.state = {course:this.props.course};
		}
		changeSubject(chap,sub){
			console.log("chnage sub");
			var course = this.state.course;
			var found = false;
			for(var i =0; i< course.subjects.length;i++){
				if(course.subjects[i].id == sub.id){
					if(course.subjects[i].chapters==null)
						course.subjects[i].chapters= [];
					for(var j = 0; j< course.subjects[i].chapters.length;j++){
						if(course.subjects[i].chapters[j].id == chap.id){
							course.subjects[i].chapters[j] = chap;
							console.log("fond sub");
							found = true;
							break; 
						}
					}
					if(!found)
						course.subjects[i].chapters.push(chap);
				}
			}
			
				
			console.log(chap);
			this.setState({course:course});
		}

		deleteChapter(chapId){
			var course = this.state.course;
			var found = false;
			for(var i =0; i< course.subjects.length;i++){
		
				for(var j = 0; j< course.subjects[i].chapters.length;j++){
					if(course.subjects[i].chapters[j].id == chapId){
						course.subjects[i].chapters[j] = chap;
						course.subjects[i].chapters.splice(j,1);
						break;
						break; 
					}
				}
				
			}
			this.setState({course:course});
		}
		addChapter(sub){
			ReactDOM.render(
			<AddChapter courseUrl = {this.state.course.url} subject={sub} changeChapter ={this.changeSubject.bind(this)} />,
			document.getElementById("addtopic")
			);
			document.getElementById("addtopic").setAttribute("data-state","open");
		}
		render(){
		
			return(
				<div>
				{this.state.course.subjects.map((sub,index)=>
				<div className="breadcrumb">
					<ul>
						<li><h2>{sub.title}</h2></li>
						<button onClick={this.addChapter.bind(this,sub)}>Add chapter</button>
					</ul>
					
					{sub.chapters!=null && sub.chapters.map((chap,index)=>	
					<div>
						<Chapter chapter = {chap}  subject = {sub}  courseUrl = {this.state.course.url} changeSubject = {this.changeSubject.bind(this)} deleteChapter={this.deleteChapter.bind(this)}/>
					</div>
				
					)}		
					
				</div>
				
				)}
				</div>
			);
		}
		

	}

	class Chapter extends React.Component{
		constructor(props){
			super(props);
		}
		changeTopic(topic){
			console.log("called");
			var chap = this.props.chapter;
			var found = false;
			if(chap.topics==null){
				chap.topics = [];
			}
			for(var i = 0; i< chap.topics.length;i++){
				if(chap.topics[i].id == topic.id){
					chap.topics[i] = topic;	
					found = true;
					console.log("found");
				}
			}
			if(!found){
				chap.topics.push(topic);
			}
			console.log(chap);
			this.props.changeSubject(chap,this.props.subject);
		}
		

		deleteTopic(topicId){
			console.log("foind 23");
			console.log(topicId);
			for(var i = 0; i< chap.topics.length;i++){
				console.log(chap.topics[i].id);
				if(chap.topics[i].id == topicId){
					//chap.topics[i] = topic;	
					chap.topics.splice(i,1);
					console.log("found");

				}
			}
			this.props.changeSubject(chap,this.props.subject);
		}
		changeChapter(chap,sub){
			this.props.changeSubject(chap,sub);
		}
		deleteChapter(chapId){
			this.props.deleteChapter(chapId);
		}



		addTopic(){
			ReactDOM.render(
			<AddTopic chapter={this.props.chapter} courseUrl = {this.props.courseUrl} subject={this.props.subject} changeTopic ={this.changeTopic.bind(this)} />,
			document.getElementById("addtopic")
			);
			document.getElementById("addtopic").setAttribute("data-state","open");
		}

		editChapter(){
			ReactDOM.render(
			<AddChapter chapter={this.props.chapter} courseUrl = {this.props.courseUrl} subject={this.props.subject} changeChapter ={this.changeChapter.bind(this)}  deleteChapter={this.deleteChapter.bind(this)}/>,
			document.getElementById("addtopic")
			);
			document.getElementById("addtopic").setAttribute("data-state","open");
		}
		render(){
			if(this.props.chapter.topics!=null){
			return(
				<div>
				
				<ol class="topics">
						<li class="each-topic">
							<h3>{this.props.chapter.title}</h3>
						</li>
						<button className = "btn" onClick = {this.editChapter.bind(this)}>Edit chapter</button>
						{this.props.chapter.topics.map((topic,index)=>
						<Topic topic = {topic} chap = {this.props.chapter} courseUrl = {this.props.courseUrl} sub = {this.props.subject} changeTopic = {this.changeTopic.bind(this)} deleteTopic = {this.deleteTopic.bind(this)}/>
						)}
						<button className = "btn" onClick = {this.addTopic.bind(this)}>Add topic</button>
				</ol>
				</div>
			);
			}
			else{
				return(
				<div>
				
				<ol class="topics">
						<li class="each-topic">
							<h3>{this.props.chapter.title}</h3>
						</li>
						<button className = "btn" onClick = {this.editChapter.bind(this)}>Edit chapter</button>
						<button className = "btn" onClick = {this.addTopic.bind(this)}>Add topic</button>
				</ol>
				</div>
				);
			}
		}
	}
	class Topic extends React.Component{
		constructor(props){
			super(props);
		}
		editTopic(){
			ReactDOM.render(
			<AddTopic courseUrl = {this.props.courseUrl} topic = {this.props.topic} chapter={this.props.chap} subject={this.props.sub} changeTopic ={this.changeTopic.bind(this)} deleteTopic ={this.deleteTopic.bind(this)}/>,
			document.getElementById("addtopic")
			);
			document.getElementById("addtopic").setAttribute("data-state","open");
		}
		changeTopic(topic){
			this.props.changeTopic(topic);
		}
		deleteTopic(topicId){
			this.props.deleteTopic(topicId);
		}
		render(){
			return(

				<ol>
					<li>
					<a href={"/content/"+this.props.courseUrl+"/"+this.props.sub.url+"/"+this.props.chap.url+"/"+this.props.topic.url} >{this.props.topic.title}</a>
					<button onClick={this.editTopic.bind(this)}>Edit</button>
					</li>			
				</ol>
			);
		}
	}
	class AddChapter extends React.Component{
		constructor(props){
			super(props);
			var chapter = props.chapter;
			if(chapter ==null)
				chapter = {}
			this.state = {chapter:chapter}
		}
		addChapter(){
			axios.post("/sp/"+this.props.courseUrl+"/"+this.props.subject.id+"/addChapter",this.state.chapter)
				.then(res =>{
					this.props.changeChapter(res.data.data,this.props.subject);
					document.getElementById("addtopic").setAttribute("data-state","closed");	
				})
				.catch(function(error){
					
				});
		}
		deleteChapter(){
			axios.post("/sp/"+this.props.courseUrl+"/"+this.props.subject.id+"/addChapter",this.state.chapter)
				.then(res =>{
					this.props.deleteChapter(this.props.chapter.id);
					document.getElementById("addtopic").setAttribute("data-state","closed");	
				})
				.catch(function(error){
					
				});	
		}
		closeModal(){
			ReactDOM.unmountComponentAtNode(document.getElementById('addtopic'));
			document.getElementById("addtopic").setAttribute("data-state","closed");
		}
		changeInput(event){
			const name = event.target.name;
			var chapter = this.state.chapter;
			chapter[name] = event.target.value;
			this.setState({chapter:chapter});
		}	

		render(){
			return(
				<div>	
  					<div className="modal__window">
    					<div className="modal__close button" data-toggle="true" onClick={this.closeModal.bind(this)}> Close </div>
    					<h2 className="modal__title" id="modalTitle">EDIT USER</h2>
   						<div className="modal-content">
								<input type="text" name = "seqId" value={this.state.chapter.seqId} onChange = {this.changeInput.bind(this)} placeholder="Add seq Id"/>
								<input type="text" name = "title" value={this.state.chapter.title} onChange = {this.changeInput.bind(this)} placeholder="Add title"/>
								<input type="text" name = "url" value={this.state.chapter.url} onChange = {this.changeInput.bind(this)} placeholder="Add url"/>
								<input type="text" name = "skills" value={this.state.chapter.skills} onChange = {this.changeInput.bind(this)} placeholder="Add skills"/>
								<input type="text" name = "logoUrl" value={this.state.chapter.logoUrl} onChange = {this.changeInput.bind(this)} placeholder="Add logoUrl"/>
						</div>
						<button className="btn" type="button" onClick={this.addChapter.bind(this)}>Add new Chapter</button>
						<button className="btn" type="button" onClick={this.deleteChapter.bind(this)}>Delete new Chapter</button>
					</div>
			
				</div>
			);
		}
	}
	
	class AddTopic extends React.Component{
		constructor(props){
			super(props);
			var topic = this.props.topic;
			if(topic==null)
				topic = {};
			this.state = {topic:topic};
		}
		addTopic(){
			axios.post("/sp/"+this.props.courseUrl+"/"+this.props.subject.id+"/"+this.props.chapter.id+"/addTopic",this.state.topic)
				.then(res =>{
					this.props.changeTopic(res.data.data);	
					document.getElementById("addtopic").setAttribute("data-state","closed");
				})
				.catch(function(error){
					
				});
		}
		deleteTopic(){
			axios.post("/cms/"+this.state.topic.id+"/deletetopic?courseUrl="+this.props.courseUrl)
			.then(res=>{
				console.log(this.props.topic.id);
				this.props.deleteTopic(this.props.topic.id);
				document.getElementById("addtopic").setAttribute("data-state","closed");	
			})
			.catch(function(error){

			});
		}
		changeInput(event){
			const name = event.target.name;
			var topic = this.state.topic;
			topic[name] = event.target.value;
			this.setState({topic:topic});
		}
		closeModal(){
			ReactDOM.unmountComponentAtNode(document.getElementById('addtopic'));
			document.getElementById("addtopic").setAttribute("data-state","closed");
		}
		
		render(){
			return(
			

  					<div className="modal__window">
    					<div className="modal__close button" data-toggle="true" onClick={this.closeModal.bind(this)}> Close </div>
    					<h2 className="modal__title" id="modalTitle">EDIT USER</h2>
   						<div className="modal-content">
								<input type="text" name = "seqId" value={this.state.topic.seqId} onChange = {this.changeInput.bind(this)} placeholder="Add seq Id"/>
								<input type="text" name = "title" value={this.state.topic.title} onChange = {this.changeInput.bind(this)} placeholder="Add title"/>
								<input type="text" name = "url" value={this.state.topic.url} onChange = {this.changeInput.bind(this)} placeholder="Add url"/>
								<input type="text" name = "skills" value={this.state.topic.skills} onChange = {this.changeInput.bind(this)} placeholder="Add skills"/>
								<input type="text" name = "logoUrl" value={this.state.topic.logoUrl} onChange = {this.changeInput.bind(this)} placeholder="Add logoUrl"/>
						</div>
						<button className="btn" type="button" onClick={this.addTopic.bind(this)}>Add new topic</button>

						<button className="btn" type="button" onClick={this.deleteTopic.bind(this)}>Delete Topic</button>
						
					</div>
		
			);
		}
	}
	ReactDOM.render(
		<CourseSkeleton course = {window.course} />,
		document.getElementById("container")
	);