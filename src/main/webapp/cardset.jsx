var React = require('react');
var ReactDOM = require('react-dom');

class ChaptersList extends React.Component{
  constructor(props){
    super(props);
    var list = this.props.chaptersList;
    if(list==null)
      list = [];
    this.state = {chaptersList:list};
  }

  updateChapter = function(chap){
    console.log("updating chapter");
  	var chaptersList = this.state.chaptersList;
  	var found = false;
  	for(var i=0; i< chaptersList.length;i++){
  		if(chaptersList[i].id == chap.id){
  			chaptersList[i] = chap;
  			found = true;
  			break;
  		}
  	}

    if(!found)
  	chaptersList.push(chap);
    console.log(chaptersList);
  	this.setState({chaptersList:chaptersList});
  }




  addChapter(chap){
  	console.log(chap);
      document.getElementById("chaptereditmodal").setAttribute("data-state","open");
      ReactDOM.render(
        <AddChapter chapter={chap} courseList = {this.props.courseList} updateChapter = {this.updateChapter.bind(this)}/>,
        document.getElementById("chaptereditmodal")
      );
  }
  clickChapter(chap,index){
    ReactDOM.unmountComponentAtNode(document.getElementById('cardsetslist'));
    ReactDOM.render(
      <CardsetsList updateChapter = {this.updateChapter.bind(this)} cardsets={chap.cardSetsList}  patronChapterId={chap.id} patronChapter={chap} index={index} />,
      document.getElementById("cardsetslist")
    );

  }




  render(){
    return(
      <div className="td-block">
		<button onClick={this.addChapter.bind(this)}> Add Chapter</button>
		<div>
            {this.state.chaptersList!=null && this.state.chaptersList.map((chap,index) =>
              <div className="individual-row"><p onClick={this.clickChapter.bind(this,chap,index)}>{chap.title}</p> <img src="#" onClick={this.addChapter.bind(this,chap)} /></div>
            )}
		</div>
		</div>

    );
  }
}

class CardsetsList extends React.Component{
  constructor(props){
    super(props);
    var cardsets = this.props.cardsets;
    if(cardsets==null){
      cardsets = [];
    }
    console.log("cardsets");
    console.log(this.props.patronChapterId);
  }

  addCardset = function(card){
    document.getElementById("chaptereditmodal").setAttribute("data-state","open");
    ReactDOM.render(
        <AddCardset cardset={card} patronChapterId={this.props.patronChapterId} updateCardset = {this.updateCardset.bind(this)}/>,
        document.getElementById("chaptereditmodal")
    );

  }

  updateCardset = function(cardset){

  var cardsets = this.props.cardsets;
  var chapter = this.props.patronChapter;
  if(cardsets==null)
    cardsets=[];
    var found = false;
    for(var i =0; i< cardsets.length;i++){
      if(cardset.id==cardsets[i].id){
        cardsets[i] = cardset;
        found = true;
      }
    }
    if(!found){
      cardsets.push(cardset);
    }
    chapter.cardSetsList = cardsets;
    console.log(chapter);
    this.props.updateChapter(chapter);

  }


  clickCardset = function(cardset){
    axios.get("/sp/getcards/"+cardset.id)
    .then(res=>{
      ReactDOM.unmountComponentAtNode(document.getElementById('cardslist'));
      ReactDOM.render(

        <CardsList cards={res.data.data} patronChapter = {this.props.patronChapter} cardset={cardset}/>,
        document.getElementById("cardslist")
      );
    }).
    catch(function(error){

    });

  }

  render(){
    return(
    <div className="td-block">
		    <button onClick={this.addCardset.bind(this)}> Add Cardset</button>
		    {this.props.cardsets!=null && 
		      <div >
	            {this.props.cardsets!=null && this.props.cardsets.map((card,index) =>
	              <div className="individual-row" onClick={this.clickCardset.bind(this,card)}>{card.title}</div>
	            )}
		      </div>
		  	}
		  	{this.props.cardsets==null && 
		  		<div>
		  			no cardsets added
		  		</div>
		  	}
		</div>
    );
  }
}

class Content extends React.Component{
  constructor(props){
    super(props);
  }

  addCard(){

    document.getElementById("chaptereditmodal").setAttribute("data-state","open");
    ReactDOM.render(
      <AddCard card = {this.props.card} patronChapter = {this.props.patronChapter} cardset={this.props.cardset} />,
      document.getElementById("chaptereditmodal")
    );
  }



  render(){
    return(
      <div>
        <button onClick={this.addCard.bind(this)}>Edit</button>
      <div dangerouslySetInnerHTML={{ __html: this.props.card.cardItem.content}}>

      </div>
      </div>
    );
  }
}

class CardsList extends React.Component{
  constructor(props){
    super(props);
    var cards = this.props.cards;
    if(cards==null)
      cards = [];
    this.state = {cards:cards};
    console.log(this.state);
  }

  addCard(card){
    document.getElementById("chaptereditmodal").setAttribute("data-state","open");
    ReactDOM.render(
      <AddCard card = {card} patronChapter = {this.props.patronChapter} cardset={this.props.cardset}/>,
      document.getElementById("chaptereditmodal")
    );
  }


  clickCard(card){
    	ReactDOM.unmountComponentAtNode(document.getElementById('contentlist'));
    ReactDOM.render(
      <Content  card={card} patronChapter = {this.props.patronChapter} cardset={this.props.cardset}/>,
      document.getElementById("contentlist")
    )

  }


  render(){
    return(
      <div className="td-block">
		<button onClick={this.addCard.bind(this)}> Add card</button>
		{this.state.cards!=null && 
			<div>
	            {this.state.cards.map((card,index) =>
	              <div className="individual-row" onClick={this.clickCard.bind(this,card)}>{card.cardItem.title}</div>
	            )}
			</div>
		}
		
		{this.state.cards==null && 
			<div>
	            no cards added 
			</div>
		}
		</div>
    );
  }

}


class AddCardset extends React.Component{
  constructor(props){
    super(props);
    var cardset = this.props.cardset;
    if(cardset==null)
      cardset={};
    this.state = {cardset:cardset};
  }


  saveCardset(){
    axios.post("/sp/savecardset/"+this.props.patronChapterId,this.state.cardset)
    .then(res=>{
      document.getElementById("chaptereditmodal").setAttribute("data-state","closed");
      ReactDOM.unmountComponentAtNode(document.getElementById('chaptereditmodal'));
        this.props.updateCardset(res.data.data);
    })
    .catch(function(error){

    });
  }

  changeInput(event){
    const name = event.target.name;
    var cardset = this.state.cardset;
    cardset[name] = event.target.value;
    this.setState({cardset:cardset});
  }

  closeCard(){
  	ReactDOM.unmountComponentAtNode(document.getElementById('chaptereditmodal'));
  	document.getElementById("chaptereditmodal").setAttribute("data-state","closed");
  }
  render(){
    return(
      <div>
      <div className="modal__window auth">
  <button className="modal__close" data-toggle="true" onClick={this.closeCard.bind(this)}>Close</button>

  <div className="content-block">

    <div className="input-blocks">
      <h3>Add Cardset</h3>
      <input type="" name="title" placeholder="Title" value={this.state.cardset.title} onChange = {this.changeInput.bind(this)} />
      <input type="" name="seqId" placeholder="seqId" value={this.state.cardset.seqId} onChange = {this.changeInput.bind(this)} />
    </div>
    <button className="btn" onClick={this.saveCardset.bind(this)} >Add Cardset</button>
    <p className="err-msg">Some error in adding subject</p>
</div>
</div>
<div className="cover"></div>
</div>

    );
  }
}


class AddCard extends React.Component{
  constructor(props){
    super(props);
    var card = this.props.card;
    if(card == null || card.cardItem==null){
      card = {};
      card.cardType = "THEORY";
      card.cardItem = {};
    }
    this.state= {card:card};
  }
  changeInput(event){
    const name = event.target.name;
    var card = this.state.card;
    card.cardItem[name] = event.target.value;
    this.setState({card:card});
  }



  saveCard(){
    console.log(this.props);
    var url;
    if(this.state.card.id!=null)
      url = '/sp/savetheorycard/'+this.props.patronChapter.subjectId+'/'+this.props.patronChapter.chapterId+'/'+this.props.patronChapter.id+'/'+this.props.cardset.id+'?cardId='+this.state.card.id;
    else
      url = '/sp/savetheorycard/'+this.props.patronChapter.subjectId+'/'+this.props.patronChapter.chapterId+'/'+this.props.patronChapter.id+'/'+this.props.cardset.id;

    axios.post(url,this.state.card.cardItem)
    .then(res=> {
    	
    		document.getElementById("chaptereditmodal").setAttribute("data-state","closed");
      		ReactDOM.unmountComponentAtNode(document.getElementById('chaptereditmodal'));
        		
    	
    })
    .catch(function(error){

    });

  }


  closeCard(){
  	ReactDOM.unmountComponentAtNode(document.getElementById('chaptereditmodal'));
  	document.getElementById("chaptereditmodal").setAttribute("data-state","closed");
  }
  render(){
    return(
      <div>
      <div className="modal__window auth">
  <button className="modal__close" data-toggle="true" onClick={this.closeCard.bind(this)}>Close</button>

  <div className="content-block">

    <div className="input-blocks">
      <h3>Add Card</h3>
      <input type="" name="title" placeholder="Title" value={this.state.card.cardItem.title} onChange = {this.changeInput.bind(this)} />
      <input type="" name="seqId" placeholder="seqId" value={this.state.card.cardItem.seqId} onChange = {this.changeInput.bind(this)} />
      <textarea name="content" value={this.state.card.cardItem.content} onChange = {this.changeInput.bind(this)}></textarea>
    </div>
    <button className="btn" onClick={this.saveCard.bind(this)}>Add Card</button>
    <p className="err-msg">Some error in adding subject</p>
</div>
</div>
<div className="cover"></div>
</div>

    );
  }

}

class AddChapter extends React.Component{
  constructor(props){
    super(props);
    var chap = this.props.chapter;
    console.log(chap);
    if(chap==null)
      chap ={};
    this.state = {chapter:chap};
    this.findSelectedThings();
  }

  changeInput(event){
    const name = event.target.name;
    var chapter = this.state.chapter;
    chapter[name] = event.target.value;
    this.setState({chapter:chapter});
    this.findSelectedThings();
  }

  findSelectedThings(){
    let courseList = this.props.courseList;
    let chap = this.state.chapter;
    chap.selectedCourseIndex=0;
    chap.selectedSubIndex=0;
    chap.selectedChapIndex=0;
    console.log(chap);
    for(var i=0; i< courseList.length;i++){
      if(courseList[i].id== chap.courseId){
        chap.selectedCourseIndex = i;
        for(var j =0; j < courseList[i].subjects.length;j++){
          if(courseList[i].subjects[j].id == chap.subjectId){
            chap.selectedSubIndex = j;
            for(var k = 0 ; k < courseList[i].subjects[j].chapters.length;k++){
              if(courseList[i].subjects[j].chapters[k].id == chap.chapterId){
                chap.selectedChapIndex = k;
              }
            }
          }
        }
      }
    }
    //console.log(chap);
   // console.log(this.props.courseList);
    this.setState({chapter:chap});
  }

  saveChapter(){
  	axios.post("/sp/savepatronchapter",this.state.chapter)
  	.then(res => {
      document.getElementById("chaptereditmodal").setAttribute("data-state","closed");
      ReactDOM.unmountComponentAtNode(document.getElementById('chaptereditmodal'));
  		this.props.updateChapter(res.data.data);
  	});
  }


closeCard(){
  	ReactDOM.unmountComponentAtNode(document.getElementById('chaptereditmodal'));
  	document.getElementById("chaptereditmodal").setAttribute("data-state","closed");
  }

  render(){
    return(
      <div>
      <div className="modal__window auth">
  <button className="modal__close" data-toggle="true" onClick={this.closeCard.bind(this)}>Close</button>

  <div className="content-block">

    <div className="input-blocks">
      <h3>Add Patron Chapter</h3>
      <input type="" name="title" placeholder="Title" value={this.state.chapter.title} onChange = {this.changeInput.bind(this)} />
      <input type="" name="seqId" placeholder="seqId" value={this.state.chapter.seqId} onChange = {this.changeInput.bind(this)} />
    </div>
    <div className="input-blocks">
      <h3>Select Course</h3>
      <select onChange={this.changeInput.bind(this)} name="courseId">
        <option value="">Select course</option>
        {this.props.courseList.map((course,index) =>
        	  <option value={course.id}>{course.title}</option>
            // {this.state.chapter.selectedCourseIndex == index ?(
            //     <option value={index} selected>{course.title}</option>
            // ):(
            //     <option value={index}>{course.title}</option>
            // )}

        )}

      </select>
      <select onChange={this.changeInput.bind(this)} name="subjectId">
        <option value="">Select subject</option>
        {this.props.courseList[this.state.chapter.selectedCourseIndex].subjects.map((subject,index) =>
            // {this.state.chapter.selectedSubIndex == index ?(
            //     <option value={index} selected>{subject.title}</option>
            // ):(
            //     <option value={index}>{subject.title}</option>
            // )}
            <option value={subject.id}>{subject.title}</option>

        )}

      </select>

      <select onChange={this.changeInput.bind(this)} name="chapterId">
        <option value="">select chapter</option>
        {this.props.courseList[this.state.chapter.selectedCourseIndex].subjects[this.state.chapter.selectedSubIndex].chapters.map((chapter,index) =>
            // {this.state.chapter.selectedChapIndex == index ?(
            //     <option value={index} selected>{chapter.title}</option>
            // ):(

            // )}
            <option value={chapter.id}>{chapter.title}</option>

        )}

      </select>

    </div>
    <button className="btn" onClick={this.saveChapter.bind(this)}>Save Chapter</button>
    <p className="err-msg">Some error in adding subject</p>
</div>
</div>
<div className="cover"></div>
</div>
    );
  }
}


console.log(window.courseList);
axios.get("/sp/getchapters")
.then(res=>{
  ReactDOM.render(
    <ChaptersList  chaptersList = {res.data.data} courseList = {window.courseList}/>,
    document.getElementById("chapterslist")
  );
})
.catch(function(error){

});
